% Modify NSF struct to ensure that there are at least X number of words per
% document

clear all; close all; clc;
load('nsf_8874.mat');
Y = data_struct.Y;
D = length(Y);
shortind = [];
for d = 1:D,
   doc = Y{d};
   if length(doc) < 10,
      shortind = [shortind, d];
   end
end
Y(shortind) = [];
data_struct.Y = Y;
data_struct.docid(shortind) = [];
data_struct.phiF(:,shortind) = [];
save(['nsf_',num2str(length(Y)),'.mat'],'data_struct');

% counts = importdata('nsf_wordcounts.csv');
% Y(shortind) = [];
% list = [];
% vocab = [];
% for d=1:length(Y),
%    if mod(d,100)==0,
%       display(['Working on Document ' ,num2str(d)]);
%    end
%    doc = Y{d};
%    list = [vocab, doc];
%    vocab = [unique(vocab), unique(list)];
% end
% 
% display([ 'Final Vocab Length = ',num2str(length(unique(vocab))) ]);

% Ensure no duplicates exist
% wordind = [];
% for dd = 1:length(shortind),
%    doc = Y{shortind(dd)};
%    wordind = [wordind, unique(doc)];
% end




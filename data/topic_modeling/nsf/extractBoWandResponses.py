#!/usr/bin/env python
""" extractBoWandResponses.py: 
Executable script to proces NSF grant abstracts corpus
  and extract bag of words representation as well as responses

Usage:  >> python extractBoWandResponses.py datadir N T P

The user specifies three parameters to filter extracted vocabulary:
    NUM_DOCUMENTS (N): 
        desired number of documents to filter 
            (useful when this is less than # available documents)
    MIN_DOC_THRESHOLD (T):
        exclude terms appearing in fewer than X docs
    MAX_DOC_PERCENTAGE (P):
        exclude terms appearing in more than X% of docs

Corpus is represented as a collection of text files.
    User must define "corpus.txt" should be in "datadir" input directory
        Each line should be a valid path to a single text file.

After processing is complete, the results are saved into ASCII files:
    1) vocab.txt 
        Each line gives a term's id number, string value, and freq. 
        e.g. "5: diplodocus(545)" should be read as 
            "term number 5, 'diplodocus', appeared 545 times in corpus"
        NB: term number is equivalent to overall rank. 
            Term 1 is most common.  Last term is least common.        
    2) wordcounts.csv
        Each line represents the count of one term in a document
        e.g. "5, 356, 2" should be read as
            "term number 356 occurs twice in document 5"
"""
__author__ = "Michael Hughes"
__copyright__ = "Copyright 2010, LIV Research Group, Brown University"
__license__ = "GPL"
__version__ = "1.0.1"
__maintainer__ = "Michael Hughes"
__email__ = "mhughes@cs.brown.edu"
__status__ = "Development"

import os
import fnmatch
import re
from operator import itemgetter
import sys
import getopt
import string
from collections import defaultdict
from collections import Counter

STOPWORDFILEPATH = './english_stopwords.txt'

def extract_word_list( text ):
    ''' Returns list of words found in String. Matches A-Za-z and 's '''
    wordPattern = "[A-Za-z]+[']*[A-Za-z]*"  
    wordlist = re.findall( wordPattern, text)
    return wordlist

def build_stopword_set( fPath=STOPWORDFILEPATH ):
    ''' Returns list of stopwords from specified file.  '''    
    SW = set()
    stopwordfile = open( fPath )
    for line in stopwordfile.readlines():
    	word = line[:-1] # remove newline
    	SW.add( word )
    return SW

def get_final_vocab_list( WC , DC, d, p, thr):
    keepers = list()
    # sortedWC : word counts corpus wide, sorted in descending order
    #   it is a list, each entry is a (termstr, wordcount) tuple
    sortedWC = sorted(WC.items(), key=itemgetter(1), reverse=True)   
    for termstr,wordcount in sortedWC:
      doccount = DC[termstr]
      if doccount > d * p:
      	continue
      if doccount >= thr:
      	keepers.append(termstr)
    return keepers

'''
Obtain list whose i-th entry is the full path to the i-th file in the corpus
'''
def buildCorpusFilepathList(DATADIR, fPattern, nMax,depth=0, doV=False):
  if depth==0:
    if doV: print DATADIR 
  nCur = 0
  goodPaths = []
  fList=os.listdir( DATADIR )
  for fName in fList:
    fPath = DATADIR + "/" + fName
    if os.path.isdir( fPath ):
      if doV: print "%s%s/" % (" "*len(DATADIR), fName) 
      gPaths = buildCorpusFilepathList( fPath, fPattern, nMax-nCur, depth+1 )
      goodPaths.extend( gPaths )
      nCur += len( gPaths )
    elif fnmatch.fnmatch( fName, fPattern):
      #print "%s examining file %s" % (offset, fName) 
      goodPaths.append( fPath )
      nCur += 1
    else:
      if doV: print "%s--skipping file %s" % (" "*len(DATADIR), fName)
    if nCur >= nMax:
      break
  return goodPaths

def buildCorpusDWCdict( fPaths, kDict ):
  DWC = defaultdict( lambda: defaultdict(int) )
  for dc in range( len(fPaths) ):
    wText = extractTextFromField( fPaths[dc], 'Abstract' )
    wlist = extract_word_list( wText.lower() )
    for w in wlist:
      if w in kDict:
        DWC[dc+1][ kDict[w]  ] += 1
  return DWC

def buildCorpusBoWdict( fPaths ):
   V = set()
   WC = defaultdict(int)
   DC = defaultdict(int)
   stopwords = build_stopword_set()
   for dc in range( len(fPaths) ):
     wText = extractTextFromField( fPaths[dc], 'Abstract' )
     wlist = extract_word_list( wText.lower() )
     wset = set(wlist)
     wset = wset.difference( stopwords )
     # only add words to vocab if not stopwords
     V = V.union( wset  )
     for w in wlist:
       if w in stopwords:
         continue 
       WC[w] += 1   	
     for w in wset:
       DC[w] += 1
   return (V, WC, DC)

# Returns a unique set of schools/programs as well as the associated document id
def buildCorpusMetadata( fPaths ):
    schools = set()
    programs = set()
    dateinfo = set()
    schoolList = list()
    progList = list()
    dateList = list()
    for dc in range( len(fPaths) ):
        fid = open( fPaths[dc])
        for fLine in fid.readlines():
            match1 = re.match(r"NSF\s*Program\s*:\s*[0-9]*\s*[^\n]*", fLine)
            match2 = re.match(r"Sponsor\s*:\s*[^\n]*",fLine)
            match3 = re.match(r"Start Date\s*:\s*\w*[^\n]*",fLine)
            if match1:
                temp = match1.group(0).rpartition(":")
                progid = re.sub("[0-9]*",'',temp[2]).lstrip().replace(' ', '_')
                programs.add(progid)
                progList.append(progid)
            if match2:
                temp = match2.group(0).rpartition(":")
                schoolid = temp[2].lstrip().replace(' ','_')
                schools.add(schoolid)
                schoolList.append(schoolid)
            if match3:
                temp = match3.group(0).rpartition(":")
                datematch = re.search("\d{4}",temp[2])
                dateList.append(datematch.group(0))
                dateinfo.add(datematch.group(0))
    
    return( schools, programs, dateinfo, schoolList, progList, dateList )

# Thresholds metadata
def threshMetadata(schools, programs, schoolList, progList, thresh):
    SL = Counter()
    PL = Counter()
    finalSchool = set()
    finalProg = set()
    for ss in range(len(schoolList)):
        SL[schoolList[ss]] += 1
    for ss in range(len(progList)):
        PL[progList[ss]] += 1
    for value1, count1 in SL.items():
        if count1 > thresh:
            finalSchool.add(value1)
    for value2, count2 in PL.items():
        if count2 > thresh:
            finalProg.add(value2)
    return (finalSchool, finalProg)

# Builds new paths given new thresholded list
def buildNewPaths(fPaths, finalSchool, finalProg, schoolList, progList):
    validSchools = set()
    newPaths = set()
    for ii in range(len(schoolList)):
        if any(schoolList[ii] in s for s in finalSchool):
            newPaths.add(fPaths[ii])
    for ii in range(len(progList)):
        if any(progList[ii] in s for s in finalProg):
            newPaths.add(fPaths[ii])
    return list(newPaths)

# Generate the new metadata text files
def generateMetadataText(DATADIR, uniqueS, uniqueP, uniqueD, slist, plist, dlist):
    featureList = list(uniqueS) + list(uniqueP) + list(uniqueD)
    strFeatures = ','.join(map(str,featureList))
    outfile = open(DATADIR + 'metadata.txt', 'w')
    outfile.write(strFeatures + '\n')
    for s in range(len(slist)):
        dd = str(s+1)
        if slist[s] in featureList:
            aa = featureList.index(slist[s])
            outfile.write( dd + ',' + str(aa+1) + ',' + '1' + '\n')
        if plist[s] in featureList:
            bb = featureList.index(plist[s])
            outfile.write( dd + ',' + str(bb+1) + ',' + '1' + '\n')
        if dlist[s] in featureList:
            cc = featureList.index(dlist[s])
            outfile.write( dd + ',' + str(cc+1) + ',' + '1' + '\n')
    outfile.close()
    print 'Wrote metadata to file: ', outfile.name

def extractTextFromField( fPath, indicatorStr):
  NN = 12
  rawtext = list()
  fid = open( fPath, 'rt')
  doCollect = 0
  for fLine in fid.readlines():
    if len(fLine) >= NN and fLine[NN] is ":":
      if indicatorStr in fLine[:NN]:
        doCollect = 2
      else:
        doCollect = 0
    if doCollect > 0:
      if doCollect == 2:
        fLine = fLine[NN+1:]
        doCollect = 1
      rawtext.append( fLine.strip() + ' ' )
  return string.join(rawtext, '')

def extractResponse( fPath, indicatorStr ):
  fid = open( fPath, 'rt')
  y = -1
  for fLine in fid.readlines():
    if indicatorStr in fLine:
      y = re.findall(r'\d+', fLine)
      L = len(y)
      if L > 1 or L < 0:
        print "ERROR: found multiple responses in file %s:" % (fPath)
        raise ValueError
      #print fLine[:-1]
      y = int( y[0] )
    if "Not Available" in fLine:
      y = -2
  fid.close()
  return y     

######################################################################### WRITE TO FILE FUNCTIONALITY
def output_wordcounts(DATADIR, DWC ):
  '''  Print the DWC to file wordcounts.csv'''
  outfile = open(DATADIR + 'wordcounts.csv', 'w')
  for docnum, docTC in DWC.items():
    for termnum, count in docTC.items():
      lineStr = str(docnum) + ','
      lineStr += str(termnum) + ','
      lineStr += str(count) + '\n'
      outfile.write( lineStr)
  outfile.close()
  print 'Wrote document wordcount CSV data to file: ', outfile.name

def output_vocab( DATADIR, terms, TC ):
  '''  Print TermStrs and TermNums to file vocab.txt '''
  outfile = open(DATADIR + 'vocab.txt', 'w')
  for rank, termstr in terms:
    outfile.write(termstr + '\n')
  outfile.close()
  print 'Wrote vocab to file: ', outfile.name

def output_corpus( DATADIR, fPaths):
  '''  Print file paths to each doc in corups to file corpus.txt '''
  outfile = open(DATADIR + 'corpus.txt', 'w')
  for f in fPaths:
    outfile.write( f + '\n')
  outfile.close()
  print 'Wrote corpus filepaths to file: ', outfile.name

def output_response( DATADIR, ys):
  '''  Print y values to file response.txt '''
  outfile = open(DATADIR + 'response.txt', 'w')
  for y in ys:
    outfile.write( str(y) + '\n')
  outfile.close()
  print 'Wrote responses to file: ', outfile.name

############################################################################  MAIN SCRIPT
def process_data(DATADIR, n, thr, p):
  # use dictionary to summarize corpus
  #   keys are document numbers, values are full text
  #   so C[d] yields the full text of document d
  indicatorStr = "Total Amt."
  fPaths = buildCorpusFilepathList(DATADIR, "a*.txt", n)

  # ------------------------------------------  Extract responses from each doc
  ys = list()
  for fPath in fPaths:
    y = extractResponse( fPath, indicatorStr )
    ys.append(y)

  # ------------------------------------------ Remove all docs that don't have valid response
  goodis = [i for i in range(len(ys)) if ys[i] > 0 ]  
  fPaths = [ fPaths[g] for g in goodis]
  ys = [ ys[g] for g in goodis]

  # ========================================   COMPUTE RAW OCCURANCE COUNTS FOR CORPUS
  # V is a set to represent the vocabulary
  # -------------------------------------------------------------    
  # WC keeps word counts for each unique vocab term in the corpus
  #   e.g. if word 'seagull' appears ten times in corpus,
  #            then WC['seagull'] = 10
  #-----------------------------------------------------------------
  # DC keeps count of how many documents each vocab term appears in
  #    if word 'hadrosaur' appears in two different documents,
  #            then DC['hadrosaur'] = 2
  (schools, programs, dateinfo, schoolList, progList, datelist) = buildCorpusMetadata( fPaths )
  (finalSchool, finalProg) = threshMetadata(schools, programs, schoolList, progList, 10)
  print len(finalSchool), ' valid schools discovered from ' , len(schoolList), ' abstracts'
  print len(finalProg), ' valid programs discovered from ',  len(progList), ' abstracts'
  (newPaths) = buildNewPaths(fPaths, finalSchool, finalProg, schoolList, progList)
  (uniqueS, uniqueP, uniqueD, slist, plist, dlist) = buildCorpusMetadata( newPaths )
  generateMetadataText(DATADIR, finalSchool, finalProg, uniqueD, slist, plist, dlist)
  print len(newPaths), ' valid documents from ' , len(fPaths), ' documents'
  (V,WC,DC) = buildCorpusBoWdict( newPaths )
  print len(V), ' unique raw vocab terms discovered.'
  print '  Now filtering out terms that are too frequent or too common...'

  # ========================================   FILTER VOCAB BY DOCUMENT COUNT
  #  keepers is a list of vocab terms satisfying the tf-idf conditions
  #    occuring in at most MAX_DOC_PERCENTAGE of total documents
  #     and also at least in MIN_DOC_THRESHOLD documents
  keepers = get_final_vocab_list( WC , DC, n, p, thr)
  print len(keepers), ' unique terms extracted as final vocabulary.'
  print '  Each appears in at least ', str(thr), ' unique documents,'
  print '         but not more than ', str(100*p), '% of documents.'
  keepDict = dict( (keepers[k], k+1) for k in range(len(keepers)) )
    
  # =========   BUILD TERMRANK -> TERMSTR DICT   ===============
  #  TermRank['sarcophagus'] gives rank of 'sarcophagus' in the corpus
  #   rank of 1 means most popular,  larger rank means less popular
  terms = zip( range(1, len(keepers)+1 ), keepers )
  TermRank = dict( [(termstr, rank) for rank, termstr in terms ] )

  # build TermCount dictionary using only popularWords
  #   TermCount[345] gives wordcount of term #345 in corpus
  TC = dict( [(TermRank[k],v) for k,v in WC.items() if (k in keepDict)] )

  # =========   BUILD WORDCOUNT by DOCUMENT DICT   ==================
  # DWC : word counts for each final vocab term within each document
  #    if word 'seagull' appears twice in document d,
  #       then DWC[d]['seagull'] = 2
  DWC = buildCorpusDWCdict( newPaths, keepDict )

  # =======   WRITE RESULTS to FILE   ========
  output_wordcounts( DATADIR, DWC )
  output_vocab( DATADIR, terms, TC )
  output_response( DATADIR, ys)
  output_corpus( DATADIR, newPaths)

def main():
# parse command line options
  try:
      opts, args = getopt.getopt(sys.argv[1:], "h", ["help"])
  except getopt.error, msg:
    print msg
    print "for help use --help"
    sys.exit(2)
  # process options
  for o, a in opts:
    if o in ("-h", "--help"):
      print __doc__
      sys.exit(0)
  # process arguments
  if len( args ) != 4:
    print "Usage: bow.py  <dataDir> <n> <thr> <p>"
  else:
    n = int( args[1] )
    thr = int( args[2] )
    p = float( args[3] )
    process_data( args[0], n, thr, p)

if __name__ == "__main__":
  main()

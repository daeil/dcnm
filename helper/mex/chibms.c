#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <mex.h>
#include "chib_iter.c"
#define WORDS prhs[0]
#define TOPICS prhs[1]
#define PRIOR prhs[2]
#define ITER prhs[3]
#define LOGPROB plhs[0]

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray*prhs[] )

    {
    /*I/O*/   
    /*srand48(1);*/    
    int i,j;
    
    mxArray *ww;
    int Ndocu=(int)mxGetNumberOfElements(WORDS),Nd;
    int *data=mxMalloc(sizeof(int)*3000);/*heuristic...*/
    
    int W=mxGetN(TOPICS);
    int T=mxGetM(TOPICS);
    double *tmp  =mxGetPr(TOPICS);
    double **tmptopic  =mxMalloc(sizeof(double*)*W);
    for(i=0; i<W; i++) {
        tmptopic[i]=mxMalloc(sizeof(double)*T);
        for(j=0; j<T; j++) {
        tmptopic[i][j]=tmp[i*T+j];
        }
        }     
    double *prior  =mxGetPr(PRIOR);

     int numiter=(int)(mxGetScalar(ITER));
     double output=0;
    for(i=0; i<Ndocu; i++) {
     ww=mxGetCell(WORDS,i);
     tmp = mxGetPr(ww);
     Nd=mxGetNumberOfElements(ww);
     for(j=0; j<Nd;j++){
          data[j]=(int)tmp[j]-1;
     }     
    output=output+chib_run(data,tmptopic,prior,T,W,Nd,numiter);
    }
    
    LOGPROB=mxCreateDoubleScalar(output);
    for(i=0; i<W; i++) {
        mxFree(tmptopic[i]);
     }
     mxFree(tmptopic);
     mxFree(data);    
    }

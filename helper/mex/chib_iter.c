/*matlab random numbers, just to check with previous matlab code*/
int matlab_randsample(double *weight,int len) {
    double *y;
    int w,yy;
        mxArray *lhs[1],*rhs[4];;
        rhs[0]=mxCreateDoubleScalar(len);
        rhs[1]=mxCreateDoubleScalar(1);
        rhs[2]=mxCreateDoubleScalar(1);
        rhs[3]=mxCreateDoubleMatrix(1,len,mxREAL);
        y=mxGetPr(rhs[3]);
        for(w=0; w<len; w++) {
            y[w]=weight[w];
            }
        w=mexCallMATLAB(1, lhs, 4,rhs , "randsample");
        yy=(int)mxGetScalar(lhs[0])-1;
        /*printf("haha:%d\n",yy);*/
        return yy;
    }      
int matlab_rand(int len) {
    int w,yy;
        mxArray *lhs[1],*rhs[1];;
        rhs[0]=mxCreateDoubleScalar(1);
        w=mexCallMATLAB(1, lhs, 1,rhs , "rand");        
        yy=(int)(floor(mxGetScalar(lhs[0])*len));
        return yy;
    }      

int randmult(double *pi, int veclength) {
  double *pi2, *piend;
  double sum = 0.0, mass;
  int cc = 0;

  piend = pi + veclength;
  for ( pi2 = pi ; pi2 < piend ; pi2 += 1 )
    sum += *pi2;
  mass = drand48() * sum;
  while (1) {
    mass -= *pi;
    if ( mass <= 0.0 ) break;
    pi += 1;
    cc ++;
  }
  return cc;
}
int randuniform(int numvalue) {
  return floor(drand48() * numvalue);
}

double gamln(double x) {
#define M_lnSqrt2PI 0.91893853320467274178
    static double gamma_series[] = {
        76.18009172947146,
        -86.50532032941677,
        24.01409824083091,
        -1.231739572450155,
        0.1208650973866179e-2,
        -0.5395239384953e-5
        };
    int i;
    double denom, x1, series;
    if(x < 0) return NAN;
    if(x == 0) return INFINITY;
    if(!finite(x)) return x;
    denom = x+1;
    x1 = x + 5.5;
    series = 1.000000000190015;
    for(i = 0; i < 6; i++) {
        series += gamma_series[i] / denom;
        denom += 1.0;
        }
    return( M_lnSqrt2PI + (x+0.5)*log(x1) - x1 + log(series/x) );
    }

void prodvector(int len, double* a, double* b,double* c) {
    int i;
    for(i=0; i<len; i++) {
        c[i]  = a[i]*b[i];
        }
    }
void addvector(int len, int* a, double* b,double* c) {
    int i;
    for(i=0; i<len; i++) {
        c[i]  = a[i]+b[i];
        }
    }

void printintvec(int Nd, int *zz,int index) {
    int t;
      for (t = 0;t<Nd;t++){
          printf("%d ",zz[t]);
        }
        printf("\n");
}

void printdoublevec(int Nd, double *zz,int index) {
    int t;
      for (t = 0;t<Nd;t++){
          printf("%f ",zz[t]);
        }
        printf("\n");
     }

int maxvector(int len, double *a) {
    int i,c=0;
    for(i=1; i<len; i++) {
        if(a[i]>a[c]){
        c=i;
        }
    }
    return c;
    }

double sumvector(int len, double *a) {
    int i;
    double c=0;
    for(i=0; i<len; i++) {
        c+=a[i];
        }
    return c;
    }

void binvector(int len_c, int *count,int len_d,int *data) {    
    int i,c=0;    
    memset(count,0,sizeof(int)*len_c);
    for(i=0; i<len_d; i++) {
         count[data[i]]+=1;
    }

    }

double logsumexp(int len,double *data){
      int i,mm=maxvector(len, data);
     double y=0;
     for(i=0; i<len; i++) {
        y=y+exp(data[i]-data[mm]);
        }
      y=log(y)+data[mm];
      return y;
}


double log_Tprob(int *zto,int *zfrom,int *Nz,int Nd,int T,int *words,double **topics, double *topic_prior){
double lp = 0,*pz,*tmp,ss;
int t,tt;
pz=mxMalloc(sizeof(double)*T);
tmp=mxMalloc(sizeof(double)*T);
addvector(T,Nz,topic_prior,tmp);

for(t = 0;t<Nd;t++){
    Nz[zfrom[t]] = Nz[zfrom[t]] - 1;
    tmp[zfrom[t]]-=1;
    prodvector(T,topics[words[t]], tmp,pz);
    ss=0;    
    lp = lp + log(pz[zto[t]])-log(sumvector(T,pz));
    Nz[zto[t]] = Nz[zto[t]] + 1;
    tmp[zto[t]]+=1;
}
mxFree(pz);
mxFree(tmp);
return lp;
}


double chib_run(int *words,double **topics,double *topic_prior,int T,int W,int Nd,int ms_iters){
int i,w,t,sweeps,BURN_ITERS = 1000,*Nz,*tmpNz,*zsNz,*zz,cc,newz,ss;
double topic_alpha =0,*pz,*tmp;
int *zstar,*zs,sprime;
double *log_Tvals;
int *Nkstar;
double log_pz,log_w_given_z,log_joint,log_evidence;
zstar = mxMalloc(sizeof(int)*Nd);
zs    = mxMalloc(sizeof(int)*Nd);
log_Tvals = mxMalloc(sizeof(double)*ms_iters);
zz=mxMalloc(sizeof(int)*Nd);
Nz=mxMalloc(sizeof(int)*T);
tmpNz=mxMalloc(sizeof(int)*T);
zsNz=mxMalloc(sizeof(int)*T);
pz=mxMalloc(sizeof(double)*T);
tmp=mxMalloc(sizeof(double)*T);
Nkstar=mxMalloc(sizeof(int)*T);

for(w=0; w<T; w++) {
   topic_alpha+=topic_prior[w];
   Nz[w]=0;
  }

/* Assign latents to words in isolation as a simple initialization*/
for( t = 0;t<Nd;t++){
    prodvector(T,topics[words[t]], topic_prior,pz);
    /*zz[t] = matlab_randsample(pz,T);*/
    zz[t] = randmult(pz,T);
    Nz[zz[t]] = Nz[zz[t]] + 1;
}


addvector(T,Nz,topic_prior,tmp);
/* Run some sweeps of Gibbs sampling*/
for(sweeps = 0;sweeps<BURN_ITERS;sweeps++){
    for (t = 0;t<Nd;t++){
        tmp[zz[t]] = tmp[zz[t]]-1;
        Nz[zz[t]] = Nz[zz[t]] - 1;
        prodvector(T,topics[words[t]], tmp,pz);
         /*zz[t] = matlab_randsample(pz,T);*/
         zz[t] = randmult(pz,T);
        Nz[zz[t]] = Nz[zz[t]] + 1;
        tmp[zz[t]] = tmp[zz[t]]+1;
    }
}

/* Find local optimim to use as z^*, "iterative conditional modes"*/
/* But don't sp} forever on this, bail out if necessary*/

for( i = 0;i<1000;i++){
    cc=0;
    for (t = 0;t<Nd;t++){
        Nz[zz[t]] = Nz[zz[t]] - 1;
        tmp[zz[t]] = tmp[zz[t]]-1;
        prodvector(T,topics[words[t]], tmp,pz);
        newz=maxvector(T,pz);
        if(newz!=zz[t]){
        cc=1;
        zz[t]=newz;
        }
        Nz[zz[t]] = Nz[zz[t]] + 1;
        tmp[zz[t]] = tmp[zz[t]]+1;
    }
    if (cc==0){
        break;
    }    
}

memcpy(zstar , zz,sizeof(int)*Nd);

/* Run Murray & Salakhutdinov algorithm*/
/* draw starting position*/
/*ss = matlab_rand(ms_iters);*/
ss = randuniform(ms_iters);
/* Draw z^(s)*/
for (t = Nd-1;t>-1;t--){
    Nz[zz[t]] = Nz[zz[t]] - 1;
    tmp[zz[t]] = tmp[zz[t]]-1;
    prodvector(T,topics[words[t]], tmp,pz);    
         /*zz[t] = matlab_randsample(pz,T);*/
         zz[t] = randmult(pz,T);
        Nz[zz[t]] = Nz[zz[t]] + 1;
        tmp[zz[t]] = tmp[zz[t]]+1;
}

memcpy(zs , zz,sizeof(int)*Nd);
memcpy(zsNz , Nz,sizeof(int)*T);

memcpy(tmpNz , Nz,sizeof(int)*T);/*Nz will be changed during log_Tprob, so make a copy for it*/
log_Tvals[ss] = log_Tprob(zstar, zz, tmpNz,Nd,T,words,topics,topic_prior);
/*printf("5:log  %d,%f\n",ss,log_Tvals[ss]);*/

/* Draw forward stuff*/
for (sprime =ss+1;sprime<ms_iters;sprime++){
    for (t = 0;t<Nd;t++){
    Nz[zz[t]] = Nz[zz[t]] - 1;
    tmp[zz[t]] = tmp[zz[t]]-1;
    prodvector(T,topics[words[t]], tmp,pz);    
    /*zz[t] = matlab_randsample(pz,T);*/
         zz[t] = randmult(pz,T);
        Nz[zz[t]] = Nz[zz[t]] + 1;
        tmp[zz[t]] = tmp[zz[t]]+1;
    }
    memcpy(tmpNz , Nz,sizeof(int)*T);
    log_Tvals[sprime] = log_Tprob(zstar, zz, tmpNz,Nd,T,words,topics,topic_prior);
    /*printf("%f,%d\n",log_Tvals[sprime],sprime);
    printintvec(Nd,zz,-sprime*10-1);    
    printintvec(T,Nz,-sprime*10-3);
    printdoublevec(T,tmp,-sprime*10-2);
    printf("po:%d,%d\n",sprime ,matlab_rand(10000));*/
}


/* Draw backward stuff*/
addvector(T,zsNz,topic_prior,tmp);
for (sprime =ss-1;sprime>-1;sprime--){
    for (t = Nd-1;t>-1;t--){
       zsNz[zs[t]] = zsNz[zs[t]] - 1;
       tmp[zs[t]] = tmp[zs[t]]-1;       
       prodvector(T,topics[words[t]], tmp,pz);    
       /*zs[t] = matlab_randsample(pz,T);*/
         zs[t] = randmult(pz,T);
        zsNz[zs[t]] = zsNz[zs[t]] + 1;
        tmp[zs[t]] = tmp[zs[t]]+1;
    }
    memcpy(tmpNz , zsNz,sizeof(int)*T);
    log_Tvals[sprime] = log_Tprob(zstar, zs, tmpNz,Nd,T,words,topics,topic_prior);
    /*printf("%f,%d\n",log_Tvals[sprime],sprime);*/
}

/* Final estimate*/

binvector(T,Nkstar,Nd,zstar); 
log_pz = gamln(topic_alpha)-gamln(Nd + topic_alpha);
for(t=0;t<T;t++){
log_pz =log_pz +gamln(Nkstar[t] + topic_prior[t])-gamln(topic_prior[t]);
}

log_w_given_z = 0;
for (t = 0;t<Nd;t++){
    log_w_given_z = log_w_given_z + log(topics[words[t]][zstar[t]]);
}

log_joint = log_pz + log_w_given_z;
log_evidence = log_joint - (logsumexp(ms_iters,log_Tvals) - log(ms_iters));

mxFree(zstar);
mxFree(zs);
mxFree(zsNz);
mxFree(log_Tvals);
mxFree(zz);
mxFree(Nz);
mxFree(pz);
mxFree(tmp);
mxFree(tmpNz);
mxFree(Nkstar);

return log_evidence;
}

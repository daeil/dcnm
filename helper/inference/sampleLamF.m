function [lambdaF] = sampleLamF(eta, muEta, F, K, a, b) 

aF = (K)*F/2 + a;
tempEta = zeros(K,1);
for k=1:K,
    tempEta(k) = (eta(:,k)-muEta)' * (eta(:,k)-muEta);
end
bF = .5 * sum(tempEta) + b;
lambdaF = gamrnd(aF, 1/bF);
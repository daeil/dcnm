function [lambdaS] = sampleLamS(muEta, F, a, b)

aS = F/2 + a;
bS = .5*sum((muEta.^2)) + b;
lambdaS = gamrnd(aS, 1/bS);
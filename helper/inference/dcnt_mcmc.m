function model = dcnt_mcmc(Y, model, algParams)

%% Initialize learned/fixed variables & parameters
K = model.param.K;
N = model.param.N;
F = model.param.F;
betaH = model.fixed.betaH;
gamA = model.fixed.gamA;
gamB = model.fixed.gamB;
phiF = model.fixed.phiF;
%lambdaV = model.fixed.lambdaV;
lambdaV = model.learned.lambdaV;
muEta = model.learned.muEta;
lambdaF = model.learned.lambdaF;
lambdaS = model.learned.lambdaS;
eta = model.learned.eta;
nkn = model.learned.nkn;
nkw = model.learned.nkw;
nk = model.learned.nk;
z = model.learned.z;
v = model.learned.v;

if model.correlations, % LEARNING CORRELATIONS
    A = model.learned.A;
    u = model.learned.u;
    lambdaA = model.learned.lambdaA;
    covU = (lambdaV * (A'*A) + eye(K));
    covV = A*A' + (1/lambdaV*eye(K));
else
    covV = (1/lambdaV*eye(K));
end
cholS = chol(covV);

for n=randperm(N),
    % Sample U, calculate mean for V
    muV = eta'*phiF(:,n);
    if model.correlations,
        if algParams.learnU,
            u(:,n) = sampleU(v(:,n), A, lambdaV, eta, phiF(:,n), covU);
        end
    end
    
    %% Sampling v
    if algParams.mcmc.numESS < 1,
        if model.correlations,
            muV = A*u(:,n) + eta'*phiF(:,n);
        else
            muV = eta'*phiF(:,n);
        end
        v(:,n) = sampleVlogit(v(:,n), muV, lambdaV, nkn(:,n), K);
    else
        v(:,n) = algParams.sampleV(v(:,n), muV, cholS, nkn(:,n), K, algParams.mcmc.numESS);
    end
    
    pi = exp(v2pi(v(:,n), model.linkFunction));
    
    % SAMPLING Z
    if algParams.learnZ,
        [nkw, nkn, nk, z{n}] = sampleZ(n, pi, Y{n}, z{n}, nkw, nkn, nk, betaH);
    end
    
    % CREATE TOPICS (APPROXIMATION TO RETROSPECTIVE MCMC FOR SPEED UP)
    if (model.correlations && ~model.truncate),       % RETRO + CORR
        [nkn, nkw, nk, eta, v, K, A, u] = retroDCNT(nkn, nkw, nk, muEta, lambdaF, lambdaV, eta, v, phiF, K, A, u, lambdaA);
        covV = A*A' + (1/lambdaV*eye(K));
        covU = (lambdaV * (A'*A) + eye(K));
        cholS = chol(covV);
    elseif (~model.correlations && ~model.truncate),  % RETRO + NO CORRELATIONS
        [nkn, nkw, nk, eta, v, K] = retroDCNT(nkn, nkw, nk, muEta, lambdaF, lambdaV, eta, v, phiF, K);
        covV = (1/lambdaV*eye(K));
        cholS = chol(covV);
    end
    muV = eta'*phiF(:,n);
    
    %% Sampling v
    if algParams.mcmc.numESS < 1,
        if model.correlations,
            muV = A*u(:,n) + eta'*phiF(:,n);
        else
            muV = eta'*phiF(:,n);
        end
        v(:,n) = sampleVlogit(v(:,n), muV, lambdaV, nkn(:,n), K);
    else
        v(:,n) = algParams.sampleV(v(:,n), muV, cholS, nkn(:,n), K, algParams.mcmc.numESS);
    end
    
end

%% Remove empty topics
if model.correlations && ~model.truncate
    [nkn, nkw, nk, eta, v, K, A, u] = removeDCNT(nkn, nkw, nk, eta, v, K, A, u);
elseif ~model.correlations && ~model.truncate
    [nkn, nkw, nk, eta, v, K] = removeDCNT(nkn, nkw, nk, eta, v, K);
end

%% Sample A, U, lambdaA
if model.correlations, % LEARNING CORRELATIONS
    % SAMPLE A
    if algParams.learnA,
        A = sampleAcorr(A, u, v, phiF, eta, lambdaA, lambdaV, K);
        model.learned.A = A;
    end
    
    % SAMPLE LAMBDA A
    if algParams.learnLamA,
        lambdaA = sampleLamA(A, K, gamA, gamB);
        model.learned.lambdaA = lambdaA;
    end
    model.learned.u = u;
    
    %% Sample eta
    if algParams.learnEta,
        eta = sampleEta(eta, v, phiF, lambdaV, muEta, lambdaF, K, F, A, u);
    end
else
    
    %% Sample eta
    if algParams.learnEta,
        eta = sampleEta(eta, v, phiF, lambdaV, muEta, lambdaF, K, F);
    end
end

%% Sample muEta
if algParams.learnMuEta,
    muEta = sampleMuEta(muEta, eta, lambdaF, lambdaS, K, F);
end

%% Sample lambdaS
if algParams.learnLamS,
    lambdaS = sampleLamS(muEta, F, gamA, gamB);
end

%% Sample lambdaF
if algParams.learnLamF,
    lambdaF = sampleLamF(eta, muEta, F, K, gamA, gamB);
end

%% Sample lambdaV
if algParams.learnLamV,
    lambdaV = sampleLamV(v, eta, phiF, gamA, gamB);
end
    

model.param.K = K;
model.learned.nkn = nkn;
model.learned.nkw = nkw;
model.learned.nk = nk;
model.learned.z = z;
model.learned.muEta = muEta;
model.learned.eta = eta;
model.learned.lambdaF = lambdaF;
model.learned.lambdaV = lambdaV;
model.learned.lambdaS = lambdaS;
model.learned.v = v;
end


function [w1, w2, nkn, R, muEta, eta, v, pi, K, A, u] = sampleR(i, Y, pi, w1, w2, wa, wb, nkn, S, R, ...
   muEta, eta, v, lamF, lamV, phiF, K, mask, trunc, lf, A, u, lambdaA)
% sampleRnp - samples BINARY RECEIVER edge assignments and creates new
% topics if necessary.
%%%%%%%%%%% INPUT %%%%%%%%%%%%%%
% n = node of interest
% Y = observed graph
% pi = mixed membership vector for node n
% w1, w2 = parameter counts used for marginalized bernoulli likelihood
% wa, wb = hyperparameters of the beta/gamma prior for the data likelihood
% nkn = global counts of community/topic assignments
% S, R = forward, backward community/topic assignments
% mueta, eta, u, v, A, lamF, lamA, lamV, phiF = dcnr model latent variables
% K = number of community/topics

% Forward Links
N = size(Y,1);
M = size(Y,3);
for m=1:M,
   rmask = mask(:,:,m);
   order = find(rmask(:,i)' == 1);
   for j = order(randperm(length(order))),
      nkn(R(j,i,m),i) = nkn(R(j,i,m),i) - 1;
      w1(S(j,i,m),R(j,i,m),m) = w1(S(j,i,m),R(j,i,m),m) - Y(j,i,m);
      w2(S(j,i,m),R(j,i,m),m) = w2(S(j,i,m),R(j,i,m),m) - (1-Y(j,i,m));
      % find posterior for z given y_ij = {1,0}
      denom = w1(S(j,i,m),:,m) + wa + w2(S(j,i,m),:,m) + wb;
      postz1 = (w1(S(j,i,m),:,m) + wa) ./ denom;
      postz2 = (w2(S(j,i,m),:,m) + wb) ./ denom;
      postz = (postz1' .^(Y(j,i,m)) .* postz2' .^(1-Y(j,i,m))) .* pi;
      postz = postz ./ sum(postz);
      
      % sample new topic and assign it towards the counts nkn
      znew = multrand(postz,1);
      
      if (znew ~= K+1) || trunc,
         R(j,i,m) = znew;
         w1(S(j,i,m),R(j,i,m),m) = w1(S(j,i,m),R(j,i,m),m) + Y(j,i,m);
         w2(S(j,i,m),R(j,i,m),m) = w2(S(j,i,m),R(j,i,m),m) + (1-Y(j,i,m));
         nkn(znew, i) = nkn(znew, i) + 1;
      elseif ~trunc
         K = K + 1;
         % sample new variables from prior
         if (nargin > 20),
            % sample u from prior
            uk = [u; randn(1,N)];
            % sample A from prior
            Ak = randn(1,K) * sqrt(1/K*lambdaA);
            A = [A, zeros(K-1,1)];
            A = [A; Ak];
            offset = Ak*uk;
            u = uk;
         else
            offset = 0;
         end
         
         etak = mvnrand(muEta, 1/lamF * eye(size(phiF,1)));
         vk = randn(1,N) * sqrt(1/(lamV)) + (offset + etak'*phiF);
         
         % create new data structures
         eta = [eta, etak];
         v = [v; vk];
         pi = exp(v2pi(v(:,i), lf));
         nkn = [nkn; zeros(1,N)];
         wtemp1 = zeros(K+1,K+1,M);
         wtemp2 = zeros(K+1,K+1,M);
         wtemp1(1:K,1:K,:) = w1;
         wtemp2(1:K,1:K,:) = w2;
         w1 = wtemp1;
         w2 = wtemp2;
         
         % final variable assignments
         nkn(znew,i) = 1;
         R(j,i,m) = znew;
         w1(S(j,i,m),R(j,i,m),m) = w1(S(j,i,m),R(j,i,m),m) + Y(j,i,m);
         w2(S(j,i,m),R(j,i,m),m) = w2(S(j,i,m),R(j,i,m),m) + (1-Y(j,i,m));
      end
   end
end

function [vd] = sampleVlogitESS(vd, muV, cholS, nkd, K, R)

for r=1:R,
   vd = vd - muV;
   tau = cholS' * randn(K,1);
   %calculate invariant distribution
   theta = rand*2*pi;
   thetaMin = theta - (2*pi);
   thetaMax = theta;
   
   nkd2 = flipud(cumsum(flipud(nkd(2:end))));
   curLL = sum( nkd(1:K) .* -log(1+exp(-(vd+muV))) + nkd2 .* -log(1 + exp((vd+muV))) );
   hh = log(rand) + curLL;
   
   while true
      vnew = vd.*cos(theta) + tau.*sin(theta);
      curLL = sum( nkd(1:K) .* -log(1+exp(-(vnew+muV))) + nkd2 .* -log(1 + exp((vnew+muV))) );
      
      if (curLL > hh),
         break;
      end
      
      if (theta > 0),
         thetaMax = theta;
      elseif (theta < 0)
         thetaMin = theta;
      else
         error('BUG DETECTED: Shrunk to current position and still not acceptable.');
      end
      theta = rand*(thetaMax - thetaMin) + thetaMin;
   end
   vd = vnew + muV;
end
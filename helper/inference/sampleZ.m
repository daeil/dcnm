function [nkw, nkd, nk, zd] = sampleZ(d, pi, wd, zd, nkw, nkd, nk, beta)

%Sample in random order through words
words  = int32(wd) - 1;
order  = int32(randperm(length(words))) - 1;
assign = int32(zd) - 1;
nkwInt = int32(nkw);
nkdInt = int32(nkd(:,d));
nkInt  = int32(nk);
values = rand(length(words),1);

sampleTopicCollapsed(order, values, pi, beta, words, assign, nkwInt, nkdInt, nkInt);

nkw = double(nkwInt);
nkd(:,d) = double(nkdInt);
nk  = double(nkInt);
zd = double(assign) + 1;
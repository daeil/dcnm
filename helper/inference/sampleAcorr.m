function [A] = sampleAcorr(A, u, v, phiF, eta, lambdaA, lambdaV, K)

for k = 1:K,
   covA = (k*lambdaA*(1/lambdaV)*eye(k) + (u(1:k,:)*u(1:k,:)'));
   covA2 = (k*lambdaA*eye(k) + lambdaV.*(u(1:k,:)*u(1:k,:)'));
   muA = covA \ (u(1:k,:) * (v(k,:)' - phiF'*eta(:,k)));
   A(k,1:k) = mvnrand(muA, inv(covA2));
end
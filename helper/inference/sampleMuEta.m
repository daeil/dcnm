function [muEta] = sampleMuEta(muEta, eta, lambdaF, lambdaS, K, F)

for f=1:F,
    muF =  sum(eta(f,:)) / (K + lambdaS/lambdaF);
    varF =  lambdaS + K*lambdaF;
    muEta(f) = normrand(muF, 1/varF, 1);
end
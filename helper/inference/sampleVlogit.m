%Metropolis-Hastings: Independence Sampler
function [v] = sampleVlogit(v, muV, lambdaV, nkd, K)

%Sample new value for v
for r=1:2,
    vNew = mvnrand(muV, 1/lambdaV*eye(K));
    
    %calculate invariant distribution
    nkd2 = flipud(cumsum(flipud(nkd(2:end))));
    phiold = nkd(1:K) .* -log(1+exp(-v)) + nkd2 .* -log(1 + exp(v));
    phinew = nkd(1:K) .* -log(1+exp(-vNew)) + nkd2 .* -log(1 + exp(vNew));
    
    %calculate acceptance ratio
    ar = phinew - phiold;
    arind = ar > log(rand(size(ar)));
    v(arind) = vNew(arind);
end
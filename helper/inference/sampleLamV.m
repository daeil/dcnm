function lambdaV = sampleLamV(v, eta, phiF, a, b)

K = size(v,1);
N = size(v,2);
aV = (K)*N/2 + a;
tempV = zeros(N,1);
for n=1:N,
   tempV(n) = (v(:,n) - (eta'*phiF(:,n)) )' ...
      * (v(:,n) - (eta' * phiF(:,n)));
end
bV = .5 * sum(tempV) + b;
lambdaV = gamrnd(aV, 1/bV);
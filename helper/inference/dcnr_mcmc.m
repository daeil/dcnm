function model = dcnr_mcmc(Y, model, algParams)

%% Initialize learned/fixed variables & parameters
K = model.param.K;
N = model.param.N;
F = model.param.F;
gamA = model.fixed.gamA;
gamB = model.fixed.gamB;
phiF = model.fixed.phiF;
muEta = model.learned.muEta;
lambdaF = model.learned.lambdaF;
lambdaS = model.learned.lambdaS;
eta = model.learned.eta;
nkn = model.learned.nkn;
v = model.learned.v;
S = model.learned.S;
R = model.learned.R;
w1 = model.learned.w1;
w2 = model.learned.w2;
%lambdaV = model.fixed.lambdaV;
lambdaV = model.learned.lambdaV;
wa = model.fixed.wa;
wb = model.fixed.wb;
mask = model.param.mask;

if model.correlations, % LEARNING CORRELATIONS
    A = model.learned.A;
    u = model.learned.u;
    lambdaA = model.learned.lambdaA;
    covU = (lambdaV * (A'*A) + eye(K));
    cholS = chol(A*A' + (1/lambdaV*eye(K)));
else
    cholS = chol(1/lambdaV*eye(K));
end

for n=randperm(N),
    % Sample U, calculate mean for V and cholesky for ESS
    if model.correlations,
        if algParams.learnU,
            u(:,n) = sampleU(v(:,n), A, lambdaV, eta, phiF(:,n), covU);
        end
    end
    muV = eta'*phiF(:,n);
    
    %% Sampling v
    if algParams.mcmc.numESS < 1
        if model.correlations,
            muV = A*u(:,n) + eta'*phiF(:,n);
        end
        v(:,n) = sampleVlogit(v(:,n), muV, lambdaV, nkn(:,n), K);
    else
        v(:,n) = algParams.sampleV(v(:,n), muV, cholS, nkn(:,n), K, algParams.mcmc.numESS);
    end
    pi = exp(v2pi(v(:,n), model.linkFunction));
    
    %% RELATIONAL MODEL (SAMPLE S,R)
    if model.correlations, % MODELING CORRELATIONS
        [w1, w2, nkn, S, muEta, eta, v, pi, K, A, u] = sampleS(n, Y, pi, w1, w2, wa, wb, nkn, ...
            S, R, muEta, eta, v, lambdaF, lambdaV, phiF, K, mask, model.truncate, model.linkFunction, A, u, lambdaA);
        [w1, w2, nkn, R, muEta, eta, v, pi, K, A, u] = sampleR(n, Y, pi, w1, w2, wa, wb, nkn, ...
            S, R, muEta, eta, v, lambdaF, lambdaV, phiF, K, mask, model.truncate, model.linkFunction, A, u, lambdaA);
        cholS = chol(A*A' + (1/lambdaV*eye(K)));
        covU = (lambdaV * (A'*A) + eye(K));
    else     % NOT MODELING CORRELATIONS
        [w1, w2, nkn, S, muEta, eta, v, pi, K] = sampleS(n, Y, pi, w1, w2, wa, wb, nkn, ...
            S, R, muEta, eta, v, lambdaF, lambdaV, phiF, K, mask, model.truncate, model.linkFunction);
        [w1, w2, nkn, R, muEta, eta, v, pi, K] = sampleR(n, Y, pi, w1, w2, wa, wb, nkn, ...
            S, R, muEta, eta, v, lambdaF, lambdaV, phiF, K, mask, model.truncate, model.linkFunction);
        cholS = chol((1/lambdaV*eye(K)));
    end
    muV = eta'*phiF(:,n);
    
    %% Sampling v
    if algParams.mcmc.numESS < 1,
        if model.correlations,
            muV = A*u(:,n) + eta'*phiF(:,n);
        end
        v(:,n) = sampleVlogit(v(:,n), muV, lambdaV, nkn(:,n), K);
    else
        v(:,n) = algParams.sampleV(v(:,n), muV, cholS, nkn(:,n), K, algParams.mcmc.numESS);
    end
end

%% Remove empty topics
if ~model.truncate,
    if model.correlations,[nkn, w1, w2, eta, v, K, A, u]= removeDCNR(nkn, w1, w2, eta, v, K, A, u);
    else [nkn, w1, w2, eta, v, K]= removeDCNR(nkn, w1, w2, eta, v, K);
    end
end

%% Sample A, U, lambdaA
if model.correlations, % LEARNING CORRELATIONS
    % SAMPLE A
    if algParams.learnA,
        A = sampleAcorr(A, u, v, phiF, eta, lambdaA, lambdaV, K);
    end
    
    % SAMPLE LAMBDA A
    if algParams.learnLamA,
        lambdaA = sampleLamA(A, K, gamA, gamB);
    end
        
    model.learned.u = u;
    model.learned.A = A;
    model.learned.lambdaA = lambdaA;
    
    %% Sample eta
    if algParams.learnEta,
        eta = sampleEta(eta, v, phiF, lambdaV, muEta, lambdaF, K, F, A, u);
    end
else
    
    %% Sample eta
    if algParams.learnEta,
        eta = sampleEta(eta, v, phiF, lambdaV, muEta, lambdaF, K, F);
    end
end

%% Sample muEta
if algParams.learnMuEta,
    muEta = sampleMuEta(muEta, eta, lambdaF, lambdaS, K, F);
end

%% Sample lambdaS
if algParams.learnLamS,
    lambdaS = sampleLamS(muEta, F, gamA, gamB);
end
    
%% Sample lambdaF
if algParams.learnLamF,
    lambdaF = sampleLamF(eta, muEta, F, K, gamA, gamB);
end

%% Sample lambdaV
if algParams.learnLamV,
    lambdaV = sampleLamV(v, eta, phiF, gamA, gamB);
end
    
model.param.K = K;
model.learned.muEta = muEta;
model.learned.eta = eta;
model.learned.lambdaF = lambdaF;
model.learned.lambdaV = lambdaV;
model.learned.lambdaS = lambdaS;
model.learned.v = v;
model.learned.nkn = nkn;
model.learned.S = S;
model.learned.R = R;
model.learned.w1 = w1;
model.learned.w2 = w2;
end


function [A] = sampleAdiag(A, u, v, phiF, eta, lambdaA, lambdaV, K)

for k=1:K,
   covA = lambdaA + (u(k,:) .* lambdaV) * u(k,:)';
   muA = covA \ (u(k,:) .* lambdaV * (v(k,:)' - phiF'*eta(:,k)));
   A(k,k) = normrand(muA, 1/covA, 1);
end
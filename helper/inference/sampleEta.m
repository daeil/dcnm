function [eta] = sampleEta(eta, v, phiF, lambdaV, muEta, lambdaF, K, F, A, u)

E = lambdaF * eye(F);
for k = 1:K,
   if (nargin > 8),
      offset = u(1:k,:)'*A(k,1:k)';
   else
      offset = 0;
   end
   covEta = E + lambdaV*(phiF*phiF');
   mu = covEta \ (lambdaV * phiF * (v(k,:)' - offset) + lambdaF*muEta);
   eta(:,k) = mvnrand(mu, inv(covEta));
end
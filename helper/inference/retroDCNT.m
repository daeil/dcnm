function [nkn, nkw, nk, eta, v, K, A, u] = retroDCNT(nkn, nkw, nk, muEta, lambdaF, lambdaV, eta, v, phiF, K, A, u, lambdaA)
%Valid approximation to the retrospective MCMC move

N = size(nkn,2);
if (sum(nkw(end,:) > 0)),
   
   K = K + 1;
   
   if (nargin > 10),
      % sample u from prior
      uk = [u; randn(1,N)];
      % sample A from prior
      Ak = randn(1,K) * sqrt(1/K*lambdaA);      
      A = [A, zeros(K-1,1)];
      A = [A; Ak];
      offset = Ak*uk;
      u = uk;
   else
      offset = 0;
   end
   
   F = size(eta,1);
   % sample eta from prior
   etak = mvnrand(muEta, 1/lambdaF * eye(F));

   % sample v from prior
   vk = randn(1,N) * sqrt(1/(lambdaV)) + (offset + etak'*phiF);
   
   % assign to dcnt struct
   eta = [eta, etak];
   v = [v; vk];
   nkw = [nkw; zeros(1,size(nkw,2))];
   nkn = [nkn; zeros(1,N)];
   nk = sum(nkw,2);
end
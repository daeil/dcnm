function lambdaA = sampleLamA(A, K, a, b) 

aA = (sum(1:K))/2 + a;
newA = repmat((1:K),K,1);
bA = .5*sum(sum(newA'.*(A.^2))) + b;
lambdaA = gamrnd(aA, 1/bA);


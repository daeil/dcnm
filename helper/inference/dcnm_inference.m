function model = dcnm_inference( data_struct, model, algParams, outParams)

tic
if algParams.currIter == 1,
   % Initialization parameters
   [model, algParams, Y] = algParams.initFunc( data_struct, model, algParams, outParams );
end

thresh = round(algParams.maxIter - algParams.maxIter*.2);
while algParams.currIter <= algParams.maxIter
   
   % Calculate log-likelihood again
   if (algParams.currIter == 1 || rem(algParams.currIter, outParams.logPrEvery)==0)
      model = dcnmLL( model );
   end
   
   % Run MCMC Sampler
   model = algParams.sampleFunc(Y, model, algParams);
   
   % Display Model Progress
   if algParams.currIter == 1 || rem(algParams.currIter, outParams.printEvery)==0
      LL = model.ll.joint(model.param.iterLL-1);
      fprintf( ' Completed % 5d/%d after %6.0f sec | logPr % .5e  | K=%d \n', ...
         algParams.currIter, algParams.maxIter, toc, LL,model.param.K );
   end
   
   % Save Parameters Progress
   if algParams.currIter > thresh
      model = saveEvery(model, algParams);
   end
      
   algParams.currIter = algParams.currIter + 1;
   % Save Current Run 
   if rem(algParams.currIter, outParams.saveEvery)==0 && algParams.currIter ~= algParams.maxIter,
      save([outParams.savedir,'/',outParams.savefid,'_',...
         num2str(outParams.seed),'_',num2str(algParams.currIter),'.mat']);
   end
   
   if algParams.currIter > algParams.maxIter,
      display('Inference is finished!');
      break;
   end
   
end % loop over sampler iterations

% Generate correlations for each document
if model.correlations,
   [model.learned.covMat, model.learned.corrMat] = genCorrMat(model, 200);
end

if algParams.maskThresh ~= 0,
   if strcmp(model.type,'topic'),
      % Do Chib Style Estimation
      model = estimateChibs(data_struct, model, algParams);   
      % Do ROC Estimation if relational
   elseif strcmp(model.type,'relational'),
      model.learned.EY = genEY(model);
      plotDCNR(data_struct.Y, model, outParams);
      savefid = [outParams.savedir,'/',outParams.savefid,'-',num2str(outParams.seed),'-roc.png'];
      %model = genROC(data_struct, model, savefid);
      model = predictAUC( data_struct, model, savefid );
   end
end

% Final Save for Posterity
model.saved = [];
model.trace = [];
save([outParams.savedir,'/',outParams.savefid,'_',...
         num2str(outParams.seed),'_final.mat'],'model');



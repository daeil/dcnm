function [ud] = sampleU(vd, A, lambdaV, eta, phiFd, covU)
% Sample per-document aux variable U, given topic weights V
%   and rest of the hierarchy
% The generative model is
%  u(:,d) ~ Normal( 0, eye(K) )
%  v(:,d)|u(:,d) ~ Normal( A*u(:,d) + b, 1/lambdaV*eye(K)  )
%   where offset b = eta'*phiF(:,d)
% So the posterior u(:,d) | v(:,d) is normal, with parameters
%    COVAR: inv( eye(K) + lambdaV*A'*A )
%     MEAN: COVAR*( lambdaV*A'*(v(:,d)-b)  )
%   See Bishop PRML p.93, equation 2.116

if ~exist( 'covU', 'var' )
    covU = eye(size(A)) + lambdaV*(A'*A);
end

muU = covU \ (lambdaV * A' * (vd - eta'*phiFd) );
ud = mvnrand(muU, inv(covU) );
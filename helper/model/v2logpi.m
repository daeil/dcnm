function [logpi] = v2logpi(  v,  SBmodelType )
% Convert real-valued stick-breaking weights v (1xK vector)
%   into log of a normalized distribution pi (1xK+1 vector, sums to one).
% INPUT
%   v :  1 x K vector
%      v(k) can have any value (-Inf, Inf)
% OUTPUT
%   logpi : 1 x K+1 vector
%      sum( exp(logpi) ) will equal 1

if ~exist( 'SBmodelType', 'var')
    SBmodelType = 'logistic';
end
K = length(v);
if size(v,1) == K
    logpi = zeros( K+1,1 );
else
    logpi = zeros( 1, K+1 );
end

switch SBmodelType
    case {'Logistic', 'logistic', 'L', 'l','logit'}
         A1 = -log( 1+exp(-v) );
         A2 = -log( 1+exp(v) );
         logpi( 1:K ) =  A1;
         logpi( 2:K+1   ) =  logpi(2:K+1) + cumsum( A2 );
    case {'Probit','probit', 'p'}
         Phi = normcdf( v );
         A1 = log(Phi);
         A2 = log(1-Phi);
         logpi( 1:K ) =  A1;
         logpi( 2:K+1   ) =  logpi(2:K+1) + cumsum( A2 );
    case {'GEM', 'gem'}
        
end
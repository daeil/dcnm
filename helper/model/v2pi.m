function logpi = v2pi(v, P)
% converts v to log values of pi using logistic stick breaking

K = size(v,1) + 1;
logpi = zeros(K,1);

if strcmp(P,'probit')
   phi1 = normcdfln(v);
   phi2 = normcdfln(-v);
   logpi(1:K-1) = phi1;
   logpi(2:K) = logpi(2:K) + cumsum(phi2,1);
else
   phi1 = -log(1 + exp(-v));
   phi2 = -log(1 + exp(v));
   logpi(1:K-1) = phi1;
   logpi(2:K) = logpi(2:K) + cumsum(phi2,1);
end
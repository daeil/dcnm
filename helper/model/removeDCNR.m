function [nkn, w1, w2, eta, v, K, A, u] = removeDCNR(nkn, w1, w2, eta, v, K, A, u)
nk = sum(nkn,2);
%K = length(nk,1)-1;
invalidK = length(find(flipud(cumsum(flipud(nk(1:K))))==0));
if (invalidK > 0),
   K = K - invalidK;
   
   if (nargin > 6),
      A = A(1:K,1:K);
      u = u(1:K,:);
   end
   
   eta = eta(:,1:K);
   v = v(1:K,:);
   nkn = nkn(1:K+1,:);
   w1 = w1(1:K+1,1:K+1,:);
   w2 = w2(1:K+1,1:K+1,:);
end
   
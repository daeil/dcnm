function [COV CORR] = genCorrMat(model, S)

K = model.param.K;
N = model.param.N;
eta = model.learned.eta;
A = model.learned.A;
lambdaV = model.learned.lambdaV;
phiF = model.fixed.phiF;

%% For E[PI]
meanV = eta'* phiF;
covV = 1/lambdaV*eye(K) + (A*A');
% Initialize data structures
temp = zeros(S,K+1);
sampcov = zeros(S,K+1,K+1);
C = zeros(N,K+1,K+1);
%sampcorr = zeros(N,K,K);
% Run Monte Carlo Estimate
display('Estimating Correlations between Latent Clusters');
for n = 1:N,
    for s=1:S,
        temp(s,:) = exp(v2pi(mvnrand(meanV(:,n), covV), model.linkFunction));
    end
    meanK = mean(temp,1);
    temp = temp - repmat(meanK, S, 1);
    for s=1:S,
        sampcov(s,:,:) = temp(s,:)' * temp(s,:);
    end
    C(n,:,:) = squeeze(mean(sampcov,1));
end
COV = squeeze(mean(C,1));
CORR = COV ./ (sqrt(diag(COV)) * sqrt(diag(COV))'); 
display('Finished Correlation Estimation');
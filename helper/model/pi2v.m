function v = pi2v(logpi, P)

K = size(logpi,1);
if strcmp(P,'probit') %Probit
   v = zeros(K-1, size(logpi,2));
   prevSticks = 0;
   for k=1:K-1,
      term1 = logpi(k) - prevSticks;
      v(k) = norminv(exp(term1));
      prevSticks = prevSticks + normcdfln(-v(k)); 
   end
else %Logistic
   v = zeros(K-1, size(logpi,2));
   temp = exp(logpi);
   temp2 = flipud(cumsum(flipud(temp)));
   for k = 1:K-1
      v(k,:) =  log(temp(k,:)) - log(temp2(k+1,:));
   end
end


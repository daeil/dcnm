function [nkn, nkw, nk, eta, v, K, A, u] = removeDCNT(nkn, nkw, nk, eta, v, K, A, u)

invalidK = length(find(flipud(cumsum(flipud(nk(1:K))))==0));
if (invalidK > 0),
   K = K - invalidK;

   if (nargin > 6),
      A = A(1:K,1:K);
      u = u(1:K,:);
   end
   
   eta = eta(:,1:K);
   v = v(1:K,:);
   nkn = nkn(1:K+1,:);
   nkw = nkw(1:K+1,:);
   nk = nk(1:K+1,:);
end
   
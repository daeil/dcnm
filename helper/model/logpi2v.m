function v = logpi2v(logpi, SquashFcnName)
% Compute real-valued, unnormalized stick break weights v for 1,2,...K
%    given K+1 length row vector logpi 
% INPUT:  
%  logpi : 1 x Kz row vector
% OUTPUT:
%  v  : 1 x Kz-1  row vector
% FORMULA:
%   v(k) = -log( sum(pi(k:K) )/pi(k) - 1 )
% uses the fact that when f(v) is the squashing function
%   \prod_{l=1}^{k-1} (1 - f(v_l) )  = sum_{m=k}^K  pi_m
% that is, we can easily determine the length of the "remaining stick"
%   used for the k-th break when we have explicit distribution pi,
%   just by summing up the entries pi(k:end)

pi = exp(logpi);

RemStick = cumsum( pi(end:-1:1) );
RemStick = RemStick(end:-1:1);

switch SquashFcnName
    case {'Logistic', 'logit', 'logistic'}
        v = -log(  RemStick(1:end-1)./pi(1:end-1) - 1 );
    case {'Probit','probit'}
        v = norminv(  pi(1:end-1)./RemStick(1:end-1) );
end



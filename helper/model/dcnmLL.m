function model = dcnmLL(model)
%% Log likelihood script
%Summary: Calculates the log likelihood for the whole model

%% Initialize Parameters
F = model.param.F;
K = model.param.K;
N = model.param.N;
c = model.param.iterLL;
gamA = model.fixed.gamA;
gamB = model.fixed.gamB;
phiF = model.fixed.phiF;

%% Variable Assignment
v = model.learned.v;
eta = model.learned.eta;
lambdaV = model.learned.lambdaV;
lambdaF = model.learned.lambdaF;
lambdaS = model.learned.lambdaS;
muEta = model.learned.muEta;
nkn = model.learned.nkn;

%% Calculate log likelihood values
lamV = log(gampdf(lambdaV, gamA, 1/gamB));
lamF = log(gampdf(lambdaF, gamA, 1/gamB));
lamS = log(gampdf(lambdaS, gamA, 1/gamB));
etaMu = log_mnormal_pdf(muEta, 0, (1/lambdaS)*eye(F));
logA = zeros(K,1);
lamA = 0;
if model.correlations, %If modeling correlations
   A = model.learned.A;
   lambdaA = model.learned.lambdaA;
   
   % p(A)
   for k=1:K,
      logA(k) = log_mnormal_pdf(A(k,1:k)', zeros(k, 1), (1/(lambdaA*k)) * eye(k));
   end

   % p(LambdaA) 
   lamA = log(gampdf(lambdaA, gamA, 1/gamB));
end
% Save log likelihoods for correlation parameters
model.ll.logLamA(c) = lamA;
model.ll.logA(c) = sum(logA);
model.ll.joint(c) = sum(logA) + lamA;

% P(eta|muEta,lambdaF)
logEtatemp = zeros(K,1); 
for k =1:K,
   logEtatemp(k) = log_mnormal_pdf(eta(:,k), muEta, diag(1./lambdaF));
end
logEta = sum(logEtatemp);

% P(V|A, u, eta, phiF, lambdaV)
logVtemp = zeros(N,1);
if model.correlations,
   covV = A*A' + 1/lambdaV .* eye(K);
else
   covV = 1/lambdaV .* eye(K);
end

for n=1:N,
   logVtemp(n) = log_mnormal_pdf(v(:,n), eta'*phiF(:,n), covV);
end

logV = sum(logVtemp);

% P(Z|V)
logpi = zeros(K+1,N);
for n=1:N,
   logpi(:,n) = v2pi(v(:,n),model.linkFunction);
end
logZ = sum(sum(nkn .* logpi));

% P(Y|Z)
if strcmp(model.type,'topic'),
   betaH = model.fixed.betaH;
   numV = model.param.numV;
   nkw = model.learned.nkw;
   nk = model.learned.nk;
   logYtemp = zeros(K,1);
   for k=1:K+1
      logYtemp(k) = sum(gammaln(betaH + nkw(k,:))) - gammaln(betaH*numV + nk(k));
   end
   logY = sum(logYtemp);
   
elseif strcmp(model.type,'relational'),
   w1 = model.learned.w1;
   w2 = model.learned.w2;
   wa = model.fixed.wa;
   wb = model.fixed.wb;
   M = model.param.M;
   logYtemp = zeros(K+1,K+1,M);
   for m=1:M,
      logYtemp(:,:,m) = betaln(w1(:,:,m)+wa, w2(:,:,m)+wb) - betaln(wa,wb);
   end
   logY = sum(sum(sum(logYtemp)));

end

% Store trace plots of individual log likelihood values
model.ll.logLamF(c) = lamF;
model.ll.logLamV(c) = lamV;
model.ll.logLamS(c) = lamS;
model.ll.logMuEta(c) = etaMu;
model.ll.logEta(c) = logEta;
model.ll.logY(c) = logY;
model.ll.logV(c) = logV;
model.ll.logZ(c) = logZ;

% Calculate joint likelihood of the model
model.ll.joint(c) = model.ll.joint(c) + ...
   model.ll.logLamF(c) + ...
   model.ll.logLamV(c) + ...
   model.ll.logLamS(c) + ....
   model.ll.logMuEta(c) + ...
   model.ll.logEta(c) + ...
   model.ll.logV(c) + ...
   model.ll.logZ(c) + ...
   model.ll.logY(c);

model.param.iterLL = c + 1;
function p = normcdf(x)
%NORMCDF   Normal Cumulative Density Function.
% P = NORMCDF(X) returns the probability that a standard normal variate will
% be less than X.
%
% P = NORMCDF(X,M,S) returns the probability that a normal variate with
% mean M and standard deviation S will be less than x.

p = 0.5*erf(x/sqrt(2)) + 0.5;

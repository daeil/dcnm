% A chibs estimator to determine the log probability of a new document
function [logfinal, chib] = chibsDCNT(model, algParams, wd, tw, phiF, N, S)
% Chib estimation parameters
% N = number of times to sample v,z to high posterior
% S = length of chain for chib estimate

% Initilize topic assignments to wd from the prior
numW = length(wd);
lambdaV = model.fixed.lambdaV;
eta = model.learned.eta;
nkw = model.learned.nkw;
nk = sum(nkw,2);
beta = model.fixed.betaH;
K = size(nkw,1) - 1;
W = model.param.numV;

if model.correlations,   
   A = model.learned.A;
   L = A*A' + 1/lambdaV*eye(K);
else 
   L = 1/lambdaV * eye(K);
end
cholS = chol(L);

% Initialize 10 times and find best log joint
bestLogJ = -1e20;
tracePost = cell(1,5);
for j=1:10,
   % Initialize v,z,nkn
   muV = eta'*phiF;
   v = mvnrand(muV, L);
   pi = exp(v2pi(v, model.linkFunction));
   
   z = multrand(pi, numW);
   nkn = hist(z,1:K+1)';
   
   % Sample v,z to high posterior
   for n = 1:N,
      
      % Sample v to high posterior
      v = algParams.sampleV(v, muV, cholS, nkn, K, 1);
      pi = exp(v2pi(v, model.linkFunction));
      
      % Sample z conditioned on pi
      words  = int32(wd) - 1;
      order  = int32(randperm(length(words))) - 1;
      assign = int32(z) - 1;
      nkwInt = int32(nkw);
      nknInt = int32(nkn);
      nkInt  = int32(nk);
      values = rand(length(words),1);
      
      sampleZChibs(order, values, pi, beta, words, assign, nkwInt, nknInt, nkInt);
      
      nkn = double(nknInt);
      z = double(assign) + 1;
   end
   
   % Calculate the joint log probability of high posterior state
   logVjoint = log_mnormal_pdf(v, muV, L);
   logZjoint = sum(log(pi) .* nkn);
   logWjoint = 0;
   for t = 1:numW,
      logWjoint = logWjoint + log(tw(z(t), wd(t)));
   end
   logJ = logWjoint + logZjoint + logVjoint;
   tracePost{j}.logJ = logJ;
   tracePost{j}.logW = logWjoint;
   tracePost{j}.logZ = logZjoint;
   tracePost{j}.logV = logVjoint;
   
   if (logJ > bestLogJ),
      %display(['Choosing ',num2str(logJ),' over ',num2str(bestLogJ)]);
      vBest = v;
      piBest = pi;
      zBest = z;
      nknBest = nkn;
      bestLogJ = logJ;
   end
end

% save those values as u*,v*,pi*,z*,nkn*
vstar = vBest;
pistar = piBest;
zstar = zBest;
nknstar = nknBest;
v = vBest;
pi = piBest;
z = zBest;

% transition probabilties for z* -> z^(s)
logTZ = zeros(S,1);
logTV = zeros(S,1);

% draw starting position
ss = ceil(rand(1) * S);

% draw z^(s) to start transition chain fixed v*
words  = int32(wd) - 1;
order  = int32(numW:-1:1) - 1;
assign = int32(z) - 1;
nkwInt = int32(nkw);
nknInt = int32(nknstar);
nkInt  = int32(nk);
values = rand(length(words),1);

sampleZChibs(order, values, pi, beta, words, assign, nkwInt, nknInt, nkInt);

nkn = double(nknInt);
z = double(assign) + 1;

% draw v^(s) to start chain (v must be accepted)
v = algParams.sampleV(v, muV, cholS, nkn, K, 1);

% Evaluate transition probability from v^s,z^s -> to v*,z^s
vstarp = log_mnormal_pdf(vstar, muV, L);
logV = @(vold, nkn) logPFunc(vold, nkn, vstar, K);
logTV(ss) = logV(v, nkn) + vstarp;

% Evaluate transition probability from v*,z^s -> to v*,z*
logT = @(pi) logTFunc(pi, zstar, wd, tw);
logTZ(ss) = logT(pistar);

% copy for the forward chain
zf = z;
vf = v;
nknf = nkn;

% copy for the backward chain
zb = z;
vb = v;
nknb = nkn; % save for backward chain

% Start forward chain
for sprime = (ss+1):1:S,
   % Sample new value for z^(sprime)
   words  = int32(wd) - 1;
   order  = int32(1:numW) - 1;
   assign = int32(zf) - 1;
   nkwInt = int32(nkw);
   nknInt = int32(nknf);
   nkInt  = int32(nk);
   values = rand(length(words),1);
   
   sampleZChibs(order, values, pi, beta, words, assign, nkwInt, nknInt, nkInt);
   
   nknf = double(nknInt);
   zf = double(assign) + 1;
   
   % sample v again
   vf = algParams.sampleV(vf, muV, cholS, nknf, K, 1);
   pi = exp(v2pi(vf, model.linkFunction)); % calculate pi to sample z again
   
   % evaluate u*, v^s, z^s -> u*, v*, z^s
   logTV(sprime) = logV(vf, nknf) + vstarp;
   
   % evaluate u*, v*, z^s -> u*, v*, z*
   logTZ(sprime) = logT(pistar);
end

% Start backward chain
for sprime = (ss-1):-1:1,

   % sample v again
   vb = algParams.sampleV(vb, muV, cholS, nknb, K, 1);
   pi = exp(v2pi(vb, model.linkFunction)); % calculate pi to sample z again
   
   % Sample z
   words  = int32(wd) - 1;
   order  = int32(numW:-1:1) - 1;
   assign = int32(zb) - 1;
   nkwInt = int32(nkw);
   nknInt = int32(nknb);
   nkInt  = int32(nk);
   values = rand(length(words),1);
   
   sampleZChibs(order, values, pi, beta, words, assign, nkwInt, nknInt, nkInt);
   
   nknb = double(nknInt);
   zb = double(assign) + 1;

   % evaluate u^s, v^s, z^s -> u^s, v^s, z*
   logTZ(sprime) = logT(pi);
   
   % evaluate u^s, v^s, z* -> u^s, v*, z^s
   logTV(sprime) = logV(vb, nknstar) + vstarp;
   
end

% Calculate final estimate
denom = logsumexp(logTZ + logTV) - log(S);
logfinal = bestLogJ - denom;

chib.logfinal = logfinal;
chib.logjoint = logJ;
chib.logTZ = logTZ;
chib.logTV = logTV;
chib.logV = logVjoint;
chib.logW = logWjoint;
chib.logZ = logZjoint;
chib.denom = denom;
chib.ss = ss;

%display(['Final Log is: ',num2str(logfinal)]);
end

% Calculates transition to z given pi
function lp = logTFunc(pi, z, wd, tw)
W = length(wd);
lp = 0;
for w = 1:W,
   ks = z(w); % topic assignment in z* for word t
   pz = pi .*  tw(:, wd(w));
   pz = pz ./ sum(pz); % normalize to make it valid probability distribution
   lp = lp + log(pz(ks));
end
end

% Calculates transition to pi given z
function tv = logPFunc(vold, nkn, vstar, K)
%calculate acceptance ratio
nkn2 = flipud(cumsum(flipud(nkn(2:end))));
PHIOLD = nkn(1:K) .* -log(1+exp(-vold)) + nkn2 .* -log(1 + exp(vold));
PHINEW = nkn(1:K) .* -log(1+exp(-vstar)) + nkn2 .* -log(1 + exp(vstar));
LP = PHINEW - PHIOLD;
LP(LP > 0) = 0;
tv = sum(LP);
end


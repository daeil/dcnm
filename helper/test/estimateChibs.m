function model = estimateChibs(data_struct, model, algParams)

yTest = data_struct.Y(~model.param.mask);
D = length(yTest);

if ~(isfield(data_struct,'phiF')),
   display('Warning: No Feature Matrix specified for Test Documents');
   F = 1;
   testPhiF = ones(F,D);
else
   testPhiF = data_struct.phiF(:,~model.param.mask);
end

% Estimate Global Topics Distribution
nkw = model.learned.nkw;
nk = sum(nkw,2);
beta = model.fixed.betaH;
W = model.param.numV;
tw = zeros(size(nkw));
for k=1:size(nkw,1),
    tw(k,:) = log(nkw(k,:) + beta) - log(nk(k) + beta*W); 
end
tw = exp(tw);

% Begin estimating held out likelihood via Chib Estimator
logpw = zeros(D,1);
for d=1:D,
   if (mod(d,20) == 0),
      display(['Working on document ', num2str(d)]);
   end
   [lp, ~] = chibsDCNT(model, algParams, yTest{d} , tw, testPhiF(:,d), 100, 1000);
   % alpha = ones(10, 1)./ 1;
   % lplda = chibms(yTest(d), tw, alpha, 1000); 
   logpw(d) = lp;
end
model.test.chibLP = sum(logpw);
display(['Final Chib Estimate is: ',num2str(model.test.chibLP)]);

% Calculate Chib Estimator
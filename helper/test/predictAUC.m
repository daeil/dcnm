function [model] = predictAUC( data_struct, model, savefid )

% a function that generates an expectation of the data
N = model.param.N;
M = model.param.M;
%R = model.param.R;
wa = model.fixed.wa;
wb = model.fixed.wb;
% utilize the last 50 samples for E[Y]
% also the number of samples used to estimate W
numS = length(model.trace.logpi);
EY = zeros(N,M);

for mm = 1:M
  for ii=1:N      
      for jj = 1:N
         if ii == jj
            continue;
         end

         Yhat = 0;
         for sampID = 1:numS,
            W1 = squeeze( model.trace.w1{sampID}(:,:,mm) );
            W0 = squeeze( model.trace.w2{sampID}(:,:,mm) );
  
            pi_ii = exp(model.trace.logpi{sampID}(:,ii) );
            pi_jj = exp(model.trace.logpi{sampID}(:,jj) );
               
            Yhat = Yhat + sum( sum( (pi_ii*pi_jj') .* (W1+wa)./(W1+W0+wa+wb)   )  );
         end

         EY(ii,jj,mm) = Yhat./numS;
      end
  end
end
posLL = [];
negLL = [];
totalpos = 0;
for m=1:M,
   mask = model.param.mask(:,:,m);
   %check to make sure edges were even filtered
   Y = data_struct.Y(:,:,m);
   %EY = model.learned.EY(:,:,m);
   posind = (Y==1) .* (mask==0);
   negind = (Y==0) .* ((mask+eye(size(mask)))==0);
   llp = EY(find(posind==1)); %#ok<*FNDSB>
   lln = EY(find(negind==1));
   posLL = [posLL; llp];
   negLL = [negLL; lln];
   totalpos = totalpos + sum(sum(posind));
end
likelihoods = [posLL; negLL];
exps{1}.name = 'ROC';
exps{1}.positive_n = totalpos;
exps{1}.likelihoods = likelihoods;
model.test.auc = roc(savefid,exps);


% generate ROC curve
function model = genROC(data_struct, model, savefid)

totalpos = 0;
posLL = [];
negLL = [];
M = size(data_struct.Y,3);
for m=1:M,
   mask = model.param.mask(:,:,m);
   %check to make sure edges were even filtered
   Y = data_struct.Y(:,:,m);
   EY = model.learned.EY(:,:,m);
   posind = (Y==1) .* (mask==0);
   negind = (Y==0) .* ((mask+eye(size(mask)))==0);
   llp = EY(find(posind==1)); %#ok<*FNDSB>
   lln = EY(find(negind==1));
   posLL = [posLL; llp];
   negLL = [negLL; lln];
   totalpos = totalpos + sum(sum(posind));
end
likelihoods = [posLL; negLL];
exps{1}.name = 'ROC';
exps{1}.positive_n = totalpos;
exps{1}.likelihoods = likelihoods;
model.test.auc = roc(savefid,exps);
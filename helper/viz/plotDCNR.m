function plotDCNR(Y, model, outP)

if ndims(Y)>2,
   figure;
   subplot(2,1,1);
   if (model.ll.joint(end) == 0),
      model.ll.joint = model.ll.joint(1:end-1);
   end
   plot(model.ll.joint);
   title('Joint Log Likelihood')
   xlabel('Iterations');
   
   subplot(2,1,2);
   imagesc(exp(model.trace.logpi{end}));
   title('E[pi] - Mixed Memberships');
   colorbar;
   ylabel('Communities');
   xlabel('Nodes');
   
   for m=1:size(Y,3),      
      figure;
      subplot(1,2,1);
      imagesc(squeeze(Y(:,:,m)));
      title(['Original Y - Relation ',num2str(m)]); colorbar;
      
      subplot(1,2,2);
      imagesc(squeeze(model.learned.EY(:,:,m))); colorbar;
      title(['E[Y] - Relation ',num2str(m)]);
   end
else
   figure;
   subplot(2,2,1);
   imagesc(Y);
   title('Original Y'); colorbar;
   
   subplot(2,2,2);
   imagesc(model.learned.EY); colorbar;
   title('E[Y]');
   
   subplot(2,2,3);
   if (model.ll.joint(end) == 0),
      model.ll.joint = model.ll.joint(1:end-1);
   end
   plot(model.ll.joint);
   title('Joint Log Likelihood')
   xlabel('Iterations');
   
   subplot(2,2,4);
   imagesc(exp(model.trace.logpi{end}));
   title('E[pi] - Mixed Memberships');
   colorbar;
   ylabel('Communities');
   xlabel('Nodes');
end

if nargin > 2
   savefid = [outP.savedir,'/',outP.savefid,'_',num2str(outP.seed),'-EY'];
   saveas(gcf,savefid,'fig');
   saveas(gcf,savefid,'png');
end
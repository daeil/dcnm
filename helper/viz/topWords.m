function topWords(fid, nkw, vocab, numW, numTopic)

fid = fopen(fid, 'w+');
for k=1:numTopic,
   [aa bb] = sort(nkw(k,:),'descend');
   fprintf( fid, '%s', ['"Topic ',num2str(k),':  '] );
   for i=1:numW-1,
      fprintf( fid, '%s', [vocab{bb(i)},', '] );
   end
   fprintf( fid, '%s\n', [vocab{bb(numW)}] );
end
fclose(fid);
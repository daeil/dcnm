function plotDCNT(model, outP)

i = 3; j = 1;

figure;

subplot(i,j,1);
if (model.ll.joint(end) == 0),
   model.ll.joint = model.ll.joint(1:end-1);
end
plot(model.ll.joint);
title('Joint Log Likelihood')

subplot(i,j,2);
imagesc(model.learned.nkw);
title('Document x Topic Counts'); colorbar;

subplot(i,j,3);
imagesc(exp(model.learned.nkn));
title('Document x Topic Counts'); colorbar;

if nargin > 1
   savefid = [outP.savedir,'/',outP.savefid,'_',num2str(outP.seed),'-EY'];
   saveas(gcf,savefid,'fig');
   saveas(gcf,savefid,'png');
end
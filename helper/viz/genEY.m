function [EY] = genEY(model)

% a function that generates an expectation of the data
N = model.param.N;
M = model.param.M;
wa = model.fixed.wa;
wb = model.fixed.wb;
% utilize the last 50 samples for E[Y]
% also the number of samples used to estimate W
numS = length(model.trace.logpi);
start = ceil(median(1:numS));
% ==============================================  E[ Y | W, pi ]
EY = zeros(N,N,M);

for m=1:M,
   for i=1:N,
      order = (1:N);
      order(i) = [];
      for j=order,
         Yhat = 0;
         for s = start:numS,
         %for s = numS,
            w1 = squeeze(model.trace.w1{s}(:,:,m));
            w2 = squeeze(model.trace.w2{s}(:,:,m));
            pi_n = exp(model.trace.logpi{s}(:,i) );
            pi_m = exp(model.trace.logpi{s}(:,j) );
            Yhat = Yhat + sum(sum((pi_n*pi_m') .* (w1+wa)./(w1+w2+wa+wb)));
         end
         EY(i,j,m) = Yhat./numS;
         %EY(i,j,m) = Yhat;
      end
   end
end




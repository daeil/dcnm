function topDocs(fid, nkn, docid, numD, numTopic)

fid = fopen(fid, 'w+');
pi = zeros(size(nkn));
for i=1:size(nkn,2),
   pi(:,i) = nkn(:,i) ./ sum(nkn(:,i));
end

for k=1:numTopic,
   [aa bb] = sort(pi(k,:),'descend');
   fprintf( fid, '%s', ['"Topic ',num2str(k),':  '] );
   for i=1:numD-1,
      fprintf( fid, '%s\n', [num2str(pi(k,bb(i))),'%: ', docid{bb(i)},', '] );
   end
   fprintf( fid, '%s\n\n', [num2str(pi(k,bb(numD))),'%: ',docid{bb(numD)}] );
end
fclose(fid);
function settings = defaultAlgorithmParams( model )

% Create a struct encoding the default settings of the MCMC
settings.inference = 1;
settings.type = 'mcmc';
settings.maxIter = 100;
settings.currIter = 1;

if strcmp(model.type,'topic')
   settings.initFunc = @initMCMC_dcnt_seq;
   settings.sampleFunc = @dcnt_mcmc;
elseif strcmp(model.type, 'relational'),
   settings.initFunc = @initMCMC_dcnr_seq;
   settings.sampleFunc = @dcnr_mcmc;
end

% Specify a mask threshold
settings.maskThresh = 1;

% MCMC specific settings
settings.mcmc.numESS = 1;
settings.sampleV = @sampleVlogitESS;

% Change if you wish to fix certain parameters
settings.learnZ = 1;
settings.learnA = 1;
settings.learnU = 1;
settings.learnV = 1;
settings.learnLamA = 1;
settings.learnLamF = 1;
settings.learnLamS = 1;
settings.learnEta = 1;
settings.learnMuEta = 1;

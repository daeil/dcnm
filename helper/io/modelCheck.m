function [model, outParams, algParams] = modelCheck(data_struct, model, algParams, outParams)

if ~exist( outParams.savedir, 'dir' )
    [~,~] = mkdir( outParams.savedir );
end

display(['Saving results as ',outParams.savedir , '/', outParams.savefid]);
if algParams.mcmc.numESS < 1,
    display('Utilizing a MH-Indpendence sampler for V');
else
    display('Utilizing an ESS sampler for V');
end

if ~algParams.useMeta,
    N = model.param.N;
    model.fixed.phiF = ones(1,N);
    model.param.F = 1;
    display('Not utilizing Metadata');
else
    display(['Utilizing Metdata: Number of Features = ',num2str(model.param.F)]);
end

if ~algParams.learnZ,
   display('Not sampling Z...');
end

if ~algParams.learnA,
   display('Not sampling A...');
end

if ~algParams.learnV,
   display('Not sampling V...');
end

if ~algParams.learnEta,
   display('Not sampling eta...');
end

if ~algParams.learnMuEta,
   display('Not sampling MuEta...');
end

if ~algParams.learnU,
   display('Not sampling U...');
end

if ~algParams.learnLamA,
   display('Not sampling lambdaA...');
end

if ~algParams.learnLamF,
   display('Not sampling lambdaF...');
end

if ~algParams.learnLamS,
   display('Not sampling lambdaS...');
end

if ~algParams.learnLamV,
   display('Not sampling lambdaV...');
end

if model.truncate,
   display(['Model is being truncated @K=',num2str(model.param.K)]);
else
   display(['Utilizing Retrospective MCMC, initial @K=',num2str(model.param.K)]);
end

if strcmp(model.linkFunction,'probit'),
   display('Utilizing a Probit Link Function');
else
   display('Utilizing a Logistic Link Function');
end

if model.correlations,
   display('Modeling Correlations');
   if isfield(data_struct,'vocab'),
      display('Final Results will generate Hinton Diagram');
      outParams.hinton = 1;
   else
      outParams.hinton = 0;
   end
else
   display('Not Modeling Correlations');
   outParams.hinton = 0;
end

if isstruct(data_struct),
   if isfield(data_struct,'vocab'),
      display('Vocabulary Cell Array Exists: Final Results will generate Top Words by Topic');
      outParams.topwc = 1;
   else
      outParams.topwc = 0;
   end
   if isfield(data_struct,'docid'),
      display('Document List Exists: Final Results will generate Top Documents by Topic');
      outParams.topdocs = 1;
   else
      outParams.topdocs = 0;
   end
else
   display('data_struct does not exist. Please see the readme for correct application of the DCNM');
end

if model.param.F > 3,
   display('# of Features > 2: Final Results will generate Metadata Histogram');
   outParams.metadata = 1;
else
   outParams.metadata = 0;
end

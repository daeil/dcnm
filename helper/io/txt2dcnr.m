function data_struct = txt2dcnr(varargin)
% csvword, doclist, vocablist, metadata, metadatagen
inputarg = varargin{1};
optargin = size(inputarg,2);

[pathstr, name, ext] = fileparts(inputarg{1});
if (optargin >= 1) && (strcmp(ext,'.txt') || strcmp(ext,'.csv') ),
   edges = csvread(inputarg{1});
   N = max([edges(:,1) ; edges(:,2)]);
   Y = zeros(N,N);
   for e = 1:length(edges),
      ii = edges(e,1);
      jj = edges(e,2);
      Y(ii,jj) = edges(e,3);
   end
   data_struct.Y = Y;
end

%nodelabels.txt
if (optargin >= 2),
   [pathstr, name, ext] = fileparts(inputarg{2});
   if strcmp(ext,'.txt'),
      data_struct.vocab = importdata(inputarg{3});
   else
      display('No Labels for Nodes');
   end
end

%metadata.txt
if (optargin >= 3),
   [pathstr, name, ext] = fileparts(inputarg{3});
   if strcmp(ext,'.txt'),
      metadata = importdata(inputarg{3},',',1);
      % Grab metadata
      if isfield(metadata,'data'),
         featAssign = metadata.data;
         F = max(featAssign(:,2)) + 1;
         phiF = zeros(F,N);
         phiF(1,:) = 1;
         for ii = 1:size(featAssign,1),
            phiF(featAssign(ii,2)+1,featAssign(ii,1)) = featAssign(ii,3);
         end
         
         D2 = length(unique(featAssign(:,1)));
         if D2~=N,
            display('Warning: Metadata does not exist for every document');
         end
         
         if isfield(metadata,'textdata'),
            temp = regexp(metadata.textdata,',','split');
            featLabels = temp{1};
         else
            display('Warning: No labels for generative features');
            featLabels = cell(1,F-1);
            for f=1:F-1,
               featLabels{f} = ['feat',num2str(f)];
            end
         end
         data_struct.featLabels = featLabels;
      end
   end
else
   F = 1;
   phiF = ones(F,N);
   display('Constructing Default Feature Matrix');
end
data_struct.phiF = phiF;

%metadata_gen.txt
if (optargin >=4),
   [pathstr, name, ext] = fileparts(inputarg{4});
   if strcmp(ext,'.txt'),
      metadata = importdata(inputarg{4});
      % Grab metadata
      if isfield(metadata,'data'),
         featAssign = metadata.data;
         N = max(featAssign(:,1));
         phiFgen = zeros(F-1,N);
         for ii = 1:size(featAssign,1),
            phiFgen(featAssign(ii,2),featAssign(ii,1)) = featAssign(ii,3);
         end
      end
      
      if isfield(metadata,'textdata'),
         temp = regexp(metadata.textdata,',','split');
         featGenLabels = temp{1};
      else
         display('Warning: No labels for generative features');
         featGenLabels = cell(1,F-1);
         for f=1:F-1,
            featGenLabels{f} = ['feat',num2str(f)];
         end
      end
      data_struct.featGenLabels = featGenLabels;
      data_struct.phiFgen = phiFgen;
      
   end
else
   display('No metadata generation file specified');
   if (F > 2),
      display(['Generating automatic features for ',num2str(F-1),' documents']);
      phiFgen = eye(F-1);
      for f=1:F-1,
         featGenLabels{f} = ['feat',num2str(f)];
      end
      data_struct.featGenLabels = featGenLabels;
      data_struct.phiFgen = phiFgen;
   end
end
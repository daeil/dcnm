function [data_struct] = loadData_DCNM( dataFlag ) %#ok<*STOUT>

if strcmp(dataFlag,'synth_graph'),  
   error('TO DO');
else
   load(dataFlag,'data_struct');
end

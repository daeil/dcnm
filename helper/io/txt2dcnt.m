function data_struct = txt2dcnt(varargin)
% csvword, doclist, vocablist, metadata, metadatagen
inputarg = varargin{1};
optargin = size(inputarg,2);

if (optargin >= 1),
   [pathstr, name, ext] = fileparts(inputarg{1});
   if (strcmp(ext,'.txt') || strcmp(ext,'.csv') ),
      display('Importing CSV word counts');
      wordcounts = importdata(inputarg{1});
      D = max(wordcounts(:,1));
      totalW = zeros(D,1);
      Y = cell(1,D);
      for d=1:D,
         docInd = find(wordcounts(:,1)==d);
         numW = length(docInd);
         totalW(d) = sum(wordcounts(docInd,3));
         dcntDoc = [];
         for w=1:numW,
            count = wordcounts(docInd(w),3);
            wind = wordcounts(docInd(w),2);
            dcntDoc = [dcntDoc repmat(wind,1,count)];
         end
         if (mod(d,100) == 0)
            display(['Parsing Document ',num2str(d)]);
         end
         Y{d} = dcntDoc;
      end
      data_struct.Y = Y;
   end
else
   display('Must specify a word counts file in .txt or .csv for this option');
   return;
end

if (optargin >= 2),
   [pathstr, name, ext] = fileparts(inputarg{2});
   if strcmp(ext,'.txt'),
      display('Vocabulary List Imported');
      data_struct.vocab = importdata(inputarg{2});
   end
else
   display('Vocabulary not specified');
end


if (optargin >= 3),
   [pathstr, name, ext] = fileparts(inputarg{3});
   if strcmp(ext,'.txt'),
      data_struct.docid = importdata(inputarg{3});
   end
else
   display('No Document List specified');
end


if (optargin >= 4),
   [pathstr, name, ext] = fileparts(inputarg{4});
   if strcmp(ext,'.txt'),
      metadata = importdata(inputarg{4},',',1);
      % Grab metadata
      if isfield(metadata,'data'),
         display('Successfully created observed metadata matrix');
         featAssign = metadata.data;
         F = max(featAssign(:,2)) + 1;
         phiF = zeros(F,D);
         phiF(1,:) = 1;
         for ii = 1:size(featAssign,1),
            phiF(featAssign(ii,2)+1,featAssign(ii,1)) = featAssign(ii,3);
         end
         
         D2 = length(unique(featAssign(:,1)));
         if D2~=D,
            display('Warning: Metadata does not exist for every document');
         end
         
         if isfield(metadata,'textdata'),
            temp = regexp(metadata.textdata,',','split');
            featLabels = temp{1};
         else
            display('Warning: No labels for generative features');
            featLabels = cell(1,F-1);
            for f=1:F-1,
               featLabels{f} = ['feat',num2str(f)];
            end
         end
         data_struct.featLabels = featLabels;
      end
   end
else
   F = 1;
   phiF = ones(F,D);
   display('Constructing Default Feature Matrix');
end
data_struct.phiF = phiF;

if (optargin >=5),
   
   [pathstr, name, ext] = fileparts(inputarg{5});
   if strcmp(ext,'.txt'),
      metadata = importdata(inputarg{5});
      
      % Grab metadata
      if isfield(metadata,'data'),
         display('Successfully found metadata generation matrix');
         featAssign = metadata.data;
         N = max(featAssign(:,1));
         phiFgen = zeros(F-1,N);
         for ii = 1:size(featAssign,1),
            phiFgen(featAssign(ii,2),featAssign(ii,1)) = featAssign(ii,3);
         end
      end
      
      if isfield(metadata,'textdata'),
         temp = regexp(metadata.textdata,',','split');
         featGenLabels = temp{1};
      else
         display('Warning: No labels for generative features');
         featGenLabels = cell(1,F-1);
         for f=1:F-1,
            featGenLabels{f} = ['feat',num2str(f)];
         end
      end
      data_struct.featGenLabels = featGenLabels;
      data_struct.phiFgen = phiFgen;
   end
else
   display('No metadata generation file specified');
   if (F > 2),
      display(['Generating automatic features for ',num2str(F-1),' documents']);
      phiFgen = eye(F-1);
      for f=1:F-1,
         featGenLabels{f} = ['feat',num2str(f)];
      end
      data_struct.featGenLabels = featGenLabels;
      data_struct.phiFgen = phiFgen;
   end
end


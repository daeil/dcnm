function model = defaultModelParams( data_struct )

% ================================================== OBSERVATION MODEL
% Determine observation type from data
Y = data_struct.Y;

if isfield(data_struct,'obsType'),
   model.type = data_struct.obsType;
else
   if iscell(data_struct.Y),
      display('Hypothesizing that data is for a Topic Model');
      model.type = 'topic';
   elseif ismatrix(data_struct.Y(:,:,1))
      display('Hypothesizing that data is for a Relational Model');
      model.type = 'relational';
   else
      display('Specify in a variable obsType=(topic/relational)');
   end
end

switch model.type
   %% Initialize parameters for topic model
   case 'topic'
      N = length(Y);
      model.fixed.betaH = .01;
      %% Determining numV (Size of Vocabulary)
      if (isfield(data_struct,'vocab')),
         numV = length(data_struct.vocab);
      else
         % Finding maximum word index which will be vocabulary size
         display('Finding the maximum size of the vocabulary');
         currMax = 0;
         avgWords = 0;
         for n=1:N,
            newMax = max(Y{n});
            avgWords = avgWords + length(Y{n});
            if (newMax > currMax), numV = newMax; currMax = numV; end
         end
         display(['Avg. # of words per document is ',num2str(avgWords/N),' with a vocabulary size of ',num2str(numV)]);
      end
      model.param.numV = numV;
   %% Initialize some parameters for the relational model   
   case 'relational'
      N = size(Y,1);
      model.fixed.wa = .1;
      model.fixed.wb = .1;
      if ndims(Y) > 2,
         model.param.M = size(Y,3);
      else
         model.param.M = 1;
      end
   otherwise
      error( ['ERROR: Unrecognized Observation Type ' model.obsType.type ] );
end

model.truncate = 1;
model.linkFunction = 'logit';
model.correlations = 1;
model.fixed.gamA = .1;
model.fixed.gamB = .1;
model.param.N = N;
model.param.K = 10;
model.saved.K = {};

%Grab feature information from dataset
if ~(isfield(data_struct,'phiF')),
   F = 1;
   phiF = ones(F,N);
else
   phiF = data_struct.phiF;
   F = size(phiF,1);
end
model.param.F = F;
model.param.a = 1;
model.fixed.phiF = phiF;
model.fixed.lambdaV = 10;

model.learned.lambdaS = .1;
model.learned.lambdaF = .1;
model.learned.lambdaA = .1;



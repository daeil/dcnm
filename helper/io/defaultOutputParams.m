function settings = defaultOutputParams( )

settings.showToy = 0;
settings.saveEvery = 10;
settings.printEvery = 10;
settings.logPrEvery = 1;
settings.test = 0;
settings.visualization = 1;
function model = saveEvery(model, algParams)
%% Function that saves parameters in model
if strcmp(model.type,'relational'),
    % P(Z|V)
    K = model.param.K;
    N = model.param.N;
    v = model.learned.v;
    logpi = zeros(K+1,N);
    for n=1:N,
        logpi(:,n) = v2pi(v(:,n),model.linkFunction);
    end
    a = model.param.a;
    model.trace.logpi{a} = logpi;
    model.trace.w1{a} = model.learned.w1;
    model.trace.w2{a} = model.learned.w2;
    model.param.a = a + 1;
end

c = length(model.saved.K) + 1; 
model.saved.K{c} = model.param.K;

if isfield(algParams,'saveVonly') && (algParams.saveVonly == 1),
   % For large runs, looking at documents 1-10.
   model.saved.v{c} = model.learned.v(:,1:10);
else
   if model.correlations && algParams.learnA,
      model.saved.lambdaA{c} = model.learned.lambdaA;
      model.saved.u{c} = model.learned.u(:,1:10);
      model.saved.A{c} = model.learned.A;
   end

   if algParams.learnLamS; model.saved.lambdaS{c} = model.learned.lambdaS; end
   if algParams.learnLamF; model.saved.lambdaF{c} = model.learned.lambdaF; end
   
   if algParams.learnV; model.saved.v{c} = model.learned.v;  end
   if algParams.learnEta;  model.saved.eta{c} = model.learned.eta; end
   if algParams.learnMuEta; model.saved.muEta{c} = model.learned.muEta; end;
   if algParams.learnZ; model.saved.nkn{c} = model.learned.nkn(:,1:10); end
  
   if strcmp(model.type,'topic'),
      if algParams.learnZ
         model.saved.nkw{c} = model.learned.nkw;
      end
   elseif strcmp(model.type,'relational'),
      model.saved.S{c} = model.learned.S;
      model.saved.R{c} = model.learned.R;
   end
end

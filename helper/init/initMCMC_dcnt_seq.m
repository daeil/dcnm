function [model, algParams, Y] = initMCMC_dcnt_seq( data_struct, model, algParams, outParams )

display('Initializing TOPIC Model (DCNT)');

%% Initialize data structures for DCNM
Y = data_struct.Y;
N = model.param.N;
K = model.param.K;
F = model.param.F;
numV = model.param.numV;
betaH = model.fixed.betaH;

% Check to see if a mask exists
if (isfield(data_struct,'mask')) && algParams.maskThresh ~= 0,
   display('Using mask stored in data file..');
   mask = data_struct.mask;
   Y = Y(mask);
   model.fixed.phiF = model.fixed.phiF(:,mask);
   model.param.N = sum(mask);
   N = model.param.N;
elseif algParams.maskThresh > 0,
   display(['Utilizing ',num2str(algParams.maskThresh*100), '% of documents as heldout']);
   numUsed = ceil((1-algParams.maskThresh) * N);
   mask = zeros(N,1);
   temp = randperm(N);
   mask(temp(1:numUsed)) = 1; 
   mask = logical(mask);
   N = numUsed;
   model.fixed.phiF = model.fixed.phiF(:,mask);
   model.param.N = N;
   Y = Y(mask);
else
   mask = ones(N,1);
end
model.param.mask = mask;

% INITIALIZE DCNT SPECIFIC PARAMETERS
if algParams.learnZ,
   nkw = zeros(K+1,numV); % topic by word counts
   nkn = zeros(K+1,N); % topic by document counts
   nk = zeros(K+1,1);
   z = cell(1,N);
   display('Using a sequential initialization for the TOPIC model');
   alpha = 1;
   % LDA style sequential sampling
   numIter = 1;
   for n=randperm(N),
      wd = Y{n};
      numW = length(wd);
      zd = zeros(1,numW);
      for i=1:numW,
         word = wd(i);
         prior = (nkn(:,n) + alpha) ./ sum(nkn(:,n) + alpha);
         likelihood = (nkw(:,word) + betaH) ./ (nk + betaH*numV);
         topic = multrand(prior.*likelihood,1);
         nkw(topic,word) = nkw(topic,word) + 1;
         nkn(topic,n) = nkn(topic,n) + 1;
         nk(topic) = nk(topic) + 1;
         zd(i) = topic;
      end
      z{n} = zd;
      numIter = numIter + 1;
   end
   
   
   % Sort topics from largest to smallest
   [~, order] = sort(nk,'descend');
   nkn = nkn(order,:);
   nkw = nkw(order,:);
   nk = nk(order);
   for n=1:N,
      wd = Y{n};
      numW = length(wd);
      for i=1:numW,
         z{n}(i) = find(z{n}(i) == order);
      end
   end
   model.learned.nk = nk;
   model.learned.nkn = nkn;
   model.learned.nkw = nkw;
   model.learned.z = z;
end

if algParams.learnA,
   model.learned.A = eye(K);
end

if algParams.learnV,
   % Given nkd, determine pi, v, and u
   pi = zeros(K+1,N);
   v = zeros(K,N);
   for n=1:N,
      pi(:,n) = (nkn(:,n) + 1e-2) ./ sum(nkn(:,n) + 1e-2);
      v(:,n) = pi2v(log(pi(:,n)), model.linkFunction);
   end
   model.learned.v = v;
end

if algParams.learnEta,
   model.learned.eta = zeros(F,K) + repmat(mean(v,2)',F,1);
end

if algParams.learnMuEta,
   model.learned.muEta = mean(model.learned.eta,2);
end

if algParams.learnU,
   model.learned.u = zeros(K,N);
end

if algParams.learnLamA,
   model.learned.lambdaA = .1;
end

if algParams.learnLamF,
   model.learned.lambdaF = .1;
end

if algParams.learnLamS,
   model.learned.lambdaS = .1;
end

if algParams.learnLamV,
   model.learned.lambdaV = .1;
end

tempAlg = algParams;
tempAlg.truncate = 1; % truncate for burn in
display('Running MCMC-BurnIn for 100 iterations');
for ii=1:100,
   model = dcnt_mcmc(Y, model, tempAlg);
end

%% Initialize Data Structures for Trace Plots
L = length(0:outParams.logPrEvery:algParams.maxIter-1);
model.ll.joint = zeros(1,L);
model.ll.logY = zeros(1,L);
model.ll.logA = zeros(1,L);
model.ll.logV = zeros(1,L);
model.ll.logU = zeros(1,L);
model.ll.logZ = zeros(1,L);
model.ll.logLamA = zeros(1,L);
model.ll.logLamV = zeros(1,L);
model.ll.logLamS = zeros(1,L);
model.ll.logMuEta = zeros(1,L);
model.ll.logEta = zeros(1,L);
model.param.iterLL = 1;


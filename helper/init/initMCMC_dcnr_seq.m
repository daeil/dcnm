function [model, algParams, Y] = initMCMC_dcnr_seq( data_struct, model, algParams, outParams )
%% Initialize data structures for DCNM
Y = data_struct.Y;
N = model.param.N;
M = model.param.M;
K = model.param.K;
F = model.param.F;
wa = model.fixed.wa;
wb = model.fixed.wb;

%% INITIALIZATION FOR RELATIONAL MODEL (DCNR)
display('Initializing RELATIONAL Model (DCNR)');

% Check to see if a mask exists
if (isfield(data_struct,'mask')),
   display('Using mask stored in data file..');
   mask = data_struct.mask;
elseif algParams.maskThresh < 1,
   for m=1:M,
      display(['Relation ',num2str(m),' holding out ', num2str(algParams.maskThresh * 100),'% of its edges...']);
      temp = rand(N) > algParams.maskThresh;
      temp = temp - diag(diag(temp));
      mask(:,:,m) = temp;
   end
else
   mask = zeros(N,N,M);
   for m=1:M,
      display(['Relation ',num2str(m),' using 100% of its edges...']);
      temp = ones(N,N) - eye(N) - isnan(Y(:,:,m));
      mask(:,:,m) = temp;
   end
end
model.param.mask = mask;

% Initialize DCNR specific parameters
w1 = zeros(K+2,K+2,M); % suff. stats for W
w2 = zeros(K+2,K+2,M);
nkn = zeros(K+2,N);
S = zeros(N,N,M);
R = zeros(N,N,M);

display('Using a sequential initialization...');
% Set concentration parameter to put reasonable mass on each topic
alpha = 1;

% If numiter > 1, do multiple sequential passes
numIter = 2;
for iter = 1:numIter
   for m=1:M,
      for i=randperm(N),
         for j=randperm(N),
            if (mask(i,j,m)==1),
               % For multiple sequential initializations
               if iter > 1
                  w1(S(i,j,m), R(i,j,m), m) = w1(S(i,j,m), R(i,j,m), m) - Y(i,j,m);
                  w2(S(i,j,m), R(i,j,m), m) = w2(S(i,j,m), R(i,j,m), m) - (1-Y(i,j,m));
                  nkn(S(i,j,m),i) = nkn(S(i,j,m),i) - 1;
                  nkn(R(i,j,m),j) = nkn(R(i,j,m),j) - 1;
               end
               
               if Y(i,j,m)
                  L = ( w1(1:K+1,1:K+1,m)+wa  ) ./ ( w1(1:K+1,1:K+1,m) + w2(1:K+1,1:K+1,m) + wa + wb );
               else
                  L = ( w2(1:K+1,1:K+1,m)+wb  ) ./ ( w1(1:K+1,1:K+1,m) + w2(1:K+1,1:K+1,m) + wa + wb );
               end
               
               Prior = ( nkn(1:K+1,i)+alpha )*( nkn(1:K+1,j)+alpha )';
               postz = Prior .* L;
               
               znew = multrand(postz(:),1);
               snew = rem( znew-1, K+1) +1;
               rnew = ceil( znew/(K+1) );
               S(i,j,m) = snew;
               R(i,j,m) = rnew;
               
               w1(S(i,j,m), R(i,j,m), m) = w1(S(i,j,m), R(i,j,m), m) + Y(i,j,m);
               w2(S(i,j,m), R(i,j,m), m) = w2(S(i,j,m), R(i,j,m), m) + (1-Y(i,j,m));
               nkn(S(i,j,m),i) = nkn(S(i,j,m),i) + 1;
               nkn(R(i,j,m),j) = nkn(R(i,j,m),j) + 1;
            end
         end
      end
   end
end
%% Sort topic mass to favor first few topics
C = sum( nkn, 2 );
[~, sortIDs] = sort( C, 'descend' );
nkn = nkn( sortIDs, :);
for m = 1:M
   for i = 1:N
      for j = 1:N
         if mask(i,j,m) == 1
            S(i,j,m) = find( S(i,j,m) == sortIDs );
            R(i,j,m) = find( R(i,j,m) == sortIDs );
         end
      end
   end
   % Reorder suff stats for Y
   w1(:,:,m) = w1( sortIDs, sortIDs, m );
   w2(:,:,m) = w2( sortIDs, sortIDs, m );
end

model.learned.w1 = w1;
model.learned.w2 = w2;
model.learned.nkn = nkn;
model.learned.S = S;
model.learned.R = R;

if algParams.learnA,
   model.learned.A = eye(K+1);
end

if algParams.learnV,
   % Given nkd, determine pi, v, and u
   pi = zeros(K+2,N);
   v = zeros(K+1,N);
   for n=1:N,
      pi(:,n) = (nkn(:,n) + 1e-2) ./ sum(nkn(:,n) + 1e-2);
      v(:,n) = pi2v(log(pi(:,n)), model.linkFunction);
   end
   model.learned.v = v;
end

if algParams.learnEta,
   model.learned.eta = zeros(F,K+1);
end

if algParams.learnMuEta,
   model.learned.muEta = zeros(F,1);
end

if algParams.learnU,
   model.learned.u = zeros(K+1,N);
end

model.param.K = K+1;
for ii=1:10,
   model = dcnr_mcmc(Y, model, algParams);
end

%% Initialize Data Structures for Trace Plots
L = length(0:outParams.logPrEvery:algParams.maxIter-1) + 1;
model.ll.joint = zeros(1,L);
model.ll.logY = zeros(1,L);
model.ll.logA = zeros(1,L);
model.ll.logV = zeros(1,L);
model.ll.logU = zeros(1,L);
model.ll.logZ = zeros(1,L);
model.ll.logLamA = zeros(1,L);
model.ll.logLamV = zeros(1,L);
model.ll.logLamS = zeros(1,L);
model.ll.logMuEta = zeros(1,L);
model.ll.logEta = zeros(1,L);
model.param.iterLL = 1;





function model = dcnm(type, varargin)
%% RUNS THE MAIN DCNM ANALYSIS
% USAGE: model = dcnm(exptype, --options)
% exptype: 'string'
% 'text-dcnt' => Allows you to preprocess a text dataset into a dcnm struct.
%                Saved as 'data.mat' in the /dcnm_root/data folder
%                input: dcnm('text-dcnt','some_word_counts.csv', <document_list.txt>,...
%                       <vocab_list.txt>, <observed_metadata.txt>, <metadata_genfeat.txt>)
%
% 'text-dcnr' => Allows you to preprocess a relational dataset into a dcnm struct.
%                Saved as 'data.mat' in the /dcnm_root/data folder
%                input: dcnm('text-dcnr','edges.csv', <observed_metadata.txt>,...
%                       <metadata_genfeat.txt>, <node_labels.txt>)
%
% 'matfile' =>   Runs the DCNM analysis given the parameters file
%                input: dcnm('matfile', 'param_default', 'RANDOM_SEED_INTEGER')
%
% 'continue' =>  Continues the analysis from a saved dcnm struct from its last saved iteration.
%                input: dcnm('continue','matlab_results_toy_RS1_iter40.mat', <new_save_dir>)

% Add DCNM path and dependencies
[folder, name, ~] = fileparts(which('dcnm.m'));
addpath(genpath(folder));

% Check to see files are of correct format
if strcmp(type,'text-dcnt'),
   display('Preprocessing Text Documents for Topic');
   % csvword, doclist, vocablist, metadata, metadatagen
   if isempty(varargin),
      display('Must input at least Word Counts as CSV for processing.'); return;
   end
   data_struct = txt2dcnt(varargin); %#ok<NASGU>
   dataFlag = [folder,'/data/data.mat'];
   display(['Data file is stored as: ', dataFlag]);
   save(dataFlag,'data_struct');
   display(['Specify ',dataFlag, ' in your preferences to analyze this dataset.' ]);
   return;
elseif strcmp(type,'text-dcnr'),
   display('Preprocessing Text Documents for Relational Model');
   if isempty(varargin),
      display('Must input adjacency matrix as text for processing.'); return;
   end
   dataFlag = [folder,'/data/data.mat'];
   data_struct = txt2dcnr(varargin); %#ok<NASGU>
   display(['Data file is stored as: ', dataFlag]);
   save(dataFlag,'data_struct');
   display(['Specify ',dataFlag, ' in your preferences to analyze this dataset.' ]);
   return;
elseif strcmp(type, 'matfile'),
   if isempty(varargin),
      display('Must input parameter file.'); return;
   end
   display(['Utilizing Parameters found in ', varargin{1}, ' with random seed ', varargin{2}]);
   param = str2func(varargin{1});
   SEED = varargin{2};
   if ischar(SEED), SEED = ceil(str2double(SEED)); end
   % Load General Parameters from param.m
   [modelParams, algParams, outParams] = param();
   
   %% ================================================ SANITIZE INPUT
   modelParams = sanitizeUserInput( modelParams );
   outParams   = sanitizeUserInput( outParams );
   algParams   = sanitizeUserInput( algParams );
   
elseif strcmp(type, 'continue'),
   display('Continuing Analysis'),
   dataFlag = varargin{1};
   if length(varargin) == 2,
      newsavedir = varargin{2};
      display(['WARNING: Saving analysis results as: ',newsavedir]);
   end
end

%% ================================================= LOAD DATA
if ~strcmp(type,'continue'),
   data_struct = loadData_DCNM( outParams{2} );
   
   %% ================================================ INTERPRET INPUT
   model = defaultModelParams( data_struct );
   model = updateParamsWithUserInput( model, modelParams );
   
   algDefs = defaultAlgorithmParams( model );
   algParams = updateParamsWithUserInput(  algDefs, algParams );
   
   outDefs = defaultOutputParams( );
   outParams = updateParamsWithUserInput( outDefs, outParams(3:end) );
   
   %% ================================================= SET RAND NUM SEED
   % Set MATLAB's seed (takes one integer between 0 and 2^32)
   outParams.seed = mod( SEED, 2^32 );
   RandStream.setGlobalStream( RandStream( 'twister', 'Seed', outParams.seed )   );
   
   %% ================================================= CHECK EXPERIMENT
   [model, outParams, algParams] = modelCheck(data_struct, model, algParams,  outParams);
else
   load(dataFlag,'data_struct','model','algParams','outParams');
   outParams.savedir = newsavedir;
   [model, outParams, algParams] = modelCheck(data_struct, model, algParams,  outParams); %#ok<NODEF>
end


%% ================================================= RUN INFERENCE
if algParams.inference,
   model = dcnm_inference( data_struct, model, algParams, outParams );
end

%% ================================================= GENERATE VISUALIZATIONS
%% TOPIC MODEL VISUALIZATIONS
if outParams.visualization,
   if strcmp(model.type,'topic'),
      K = size(model.learned.nkn,1);
      if ~exist([outParams.savedir,'/web'],'file'),
         mkdir([outParams.savedir,'/web']);
         copyfile([folder,'/helper/viz/web/*'], [outParams.savedir,'/web'],'f');
      end
      plotDCNT(model, outParams);
      
      % Generate Word Cloud + Top Words txt
      if outParams.topwc,
         topWords([outParams.savedir,'/web/topwc.txt'], model.learned.nkw, data_struct.vocab, 100, K);
         dcntWordCloud([outParams.savedir,'/web/tw.js'], model.learned.nkw, data_struct.vocab);
      end
      
      % Generate Top Documents
      if outParams.topdocs,
         pi = model.learned.nkn;
         topDocs([outParams.savedir,'/web/topdocs.txt'], pi, data_struct.docid, 50, K);
      end
      
      % Generate hinton diagram
      if outParams.hinton,
         dcntHinton([outParams.savedir,'/web/hinton.js'], model.learned.nkw, model.learned.corrMat, data_struct.vocab, 25);
      end
      
      % Generate metadata histogram
      if outParams.metadata,
         dcntMetadata([outParams.savedir,'/web/metadata.js'], data_struct, model);
      end
      
      %% RELATIONAL MODEL VISUALIZATIONS
   elseif strcmp(model.type,'relational'),
      % TODO
   end
end

%% ================================================= HELD OUT LIKELIHOODS
if outParams.test && algParams.maskThresh ~= 1,
   if strcmp(model.type,'topic'),
      % Do Chib Style Estimation
      model = estimateChibs(data_struct, model, algParams);   
      % Do ROC Estimation if relational
   elseif strcmp(model.type,'relational'),
      model.learned.EY = genEY(model);
      plotDCNR(data_struct.Y, model, outParams);
      savefid = [outParams.savedir,'/',outParams.savefid,'-roc.png'];
      model = genROC(data_struct, model, savefid);
   end
end

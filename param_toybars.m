function [modelP, algP, outP] = param_toybars()     
% THIS IS THE PARAMETER FILE NECESSARY TO RUN THE ANALYSIS
[folder, name, ~] = fileparts(which('dcnm.m'));

%% ---- NECESSARY PARAMETERS TO MODIFY -----
% Specify the filename containing the data
% folder = current dcnm_root directory on your machine
datafid = [folder,'/data/topic_modeling/toy/toy_bars.mat'];

% If truncated, total topics will be 10
initialK = 9;

% Specify the maximum number of MCMC iterations
numiter = 500;

% Specify the save file name and directory
savefid = 'results_toybars';
savedir = [folder,'/results/toybars'];
%--------------------------------------------

% Parameters that define model settings
modelP = {...
   'param.K', initialK, ... % SPECIFY STARTING NUMBER OF CLUSTERS {Integer}
   'truncate', 1, ... % AUTOMATICALLY GROW/SHRINK CLUSTERS (RETROSPECTIVE MCMC) {0,1} OFF/ON
   'correlations', 0, ... % MODEL CORRELATIONS {0,1} OFF/ON
   'learned.lambdaV', .1, ... % SPECIFY PRECISION FOR DOCUMENTS/NODE MEMBMERSHIPS
   'learned.lambdaS', .1, ... % SPECIFY PRECISION FOR MUETA (MEAN OF ETA)
   'learned.lambdaA', .1, ... % SPECIFY PRECISION FOR A (CORRELATION FACTORS)
   'learned.lambdaF', .1, ... % SPECIFY PRECISION FOR ETA (FEATURE WEIGHTS)
   };

% Parameters that define inference/learning settings
algP = {...
   'inference', 1,... % RUNS INFERENCE {0,1} OFF / ON
   'useMeta', 1,... % ALLOWS FOR METADATA {0,1} OFF / ON
   'maxIter', numiter, ... % SPECIFY THE MAXIMUM NUMBER OF MCMC ITERATIONS
   'maskThresh', 0, ... % SPECIFY A MASK, PERCENT OF DATA POINTS NOT USED, VALUES FROM 0-1
   'learnZ', 1, ... % SAMPLE Z {0,1} DON'T SAMPLE / SAMPLE
   'learnA', 1, ... % SAMPLE A {0,1} DON'T SAMPLE / SAMPLE
   'learnU', 1, ... % SAMPLE U {0,1} DON'T SAMPLE / SAMPLE
   'learnV', 1, ... % SAMPLE V {0,1} DON'T SAMPLE / SAMPLE
   'learnEta', 1, ... % SAMPLE PRECISION OF ETA {0,1} DON'T SAMPLE / SAMPLE
   'learnMuEta', 1, ... % SAMPLE MEAN OF ETA {0,1} DON'T SAMPLE / SAMPLE
   'learnLamA', 1, ... % SAMPLE PRECISION OF A {0,1} DON'T SAMPLE / SAMPLE
   'learnLamF', 1, ... % SAMPLE PRECISION OF ETA {0,1} DON'T SAMPLE / SAMPLE
   'learnLamS', 1, ... % SAMPLE PRECISION OF MUETA {0,1} DON'T SAMPLE / SAMPLE
   'learnLamV', 1, ... % SAMPLE PRECISION OF LAMBDA_V {0,1} DON'T SAMPLE / SAMPLE
   'mcmc.numESS', 5 % NUMBER OF ELLIPTICAL SLICE SAMPLES {Integer, 0 = independence sampler} 
   };

% Parameters that define output settings
outP = {...
   'datafid', datafid,... % NAME OF DATA FILE
   'savefid', savefid,... % NAME OF SAVE FILE 
   'savedir', savedir,... % NAME OF SAVE DIRECTORY
   'saveEvery', 25,... % SAVE FILE EVERY saveEvery ITERATIONS
   'saveParamsEvery', 30,... % SAVE MODEL PARAMETERS EVERY saveParamsEvery ITERATIONS
   'logPrEvery', 5 % CALCULATE MODEL LIKELIHOOD EVERY logPrEvery ITERATIONS
   };
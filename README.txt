--------------------THE DOUBLY CORRELATED NONPARAMETRIC MODEL------------------------------

USAGE: model = dcnm(exptype, --options)
exptype: 'string', 'string-arg1', '<optional-arg2>', ...

To run an example immediately, execute this within the MATLAB command window while being 
in the dcnm_root directory which contains "dcnm.m" and "param_default.m".

EXAMPLE:    dcnm('matfile','param_toygraph', '1')

This simple example runs the DCNR (topic model) version of the DCNM that truncates the number
of topics to 10. The 'matfile' option specifies to the toolbox that the algorithm assumes a saved
dcnm mat file and that the parameters for this analysis will be stored in a function called 
"param_toygraph.m". The '1' represents the random seed and changing this will allow you to repeat
the experiments for multiple runs

EXAMPLE:    dcnm('matfile','param_toybars', '1')

This simple example runs the DCNT (topic model) version of the DCNM that truncates the number
of topics to 10. The 'matfile' option specifies to the toolbox that the algorithm assumes a saved
dcnm mat file and that the parameters for this analysis will be stored in a function called 
"param_toybars.m". The '1' represents the random seed and changing this will allow you to repeat
the experiments for multiple runs. This analysis will run for 500 iterations and will load a simple 
visualization of topic counts when the analysis ends. Further examples and usage are described below. 

------------------CREATING A VALID DCNM DATAFILE FROM TEXT FILES---------------------------

'text-dcnt' => Allows you to preprocess a text dataset into a dcnm struct. 
               Saved as 'data.mat' in the /dcnm_root/data folder 
               USAGE: dcnm('text-dcnt','some_word_counts.csv', <document_list.txt>,...
                      <vocab_list.txt>, <observed_metadata.txt>, <metadata_genfeat.txt>)

               EXAMPLE: dcnm('text-dcnt','nsf_wordcounts.csv', 'nsf_doclist.txt',...
                      'nsf_vocab.txt', 'nsf_metadata.txt', 'nsf_metadata_gen.txt')
 
INFO: "some_word_counts.csv" is a comma separated file where the first column refers
to the index of the document d, the second column refers to the unique word identifier w, and 
the third column belongs to the frequency count of that word w in document d. See "nsf_wordcounts.csv"
in the /dcnm_root/data/topic_modeling/nsf subdirectory. The rest of the arguments are optional. 
If you're missing a particular file in this order, just input '~' as the argument as the order
of the input is important. 

SOME MORE INFO: 

"document_list.txt" 
refers to a text file that contains the path to every document used in order of its document id. 
See "nsf_doclist.txt" in the /dcnm_root/data/topic_modeling/nsf subdirectory to see another example. 

"vocab_list.txt" 
refers to a text file that contains the word labels of our dictionary (example: "nsf_vocab.txt"
in the same subdirectory). 

"observed_metadata.txt" 
refers to a text file that contains the observed metadata of your dataset. The first line is assumed
to be a comma seperated line of feature labels. The second line begins the specification of feature 
tuples in this format: (document_index, feature_index, feature_value) See "nsf_metadata.txt" in the 
/dcnm_root/data/topic_modeling/nsf subdirectory to see another example.

"metadata_genfeat.txt" 
for visualization purposes using the metadata histogram visualization included in this toolbox. If you
wish to see hypothetical documents that can be generated once the model is learned, you can specify 
a metadata matrix in the form of tuples similar to the "observed_metadata.txt" file. See "nsf_metadata_gen.txt" 
in the /dcnm_root/data/topic_modeling/nsf subdirectory to see another example.


------------RELATIONAL DATA: CREATING A VALID DCNM DATAFILE FROM TEXT FILES----------------------

'text-dcnr' => Allows you to preprocess a relational dataset into a dcnm struct. 
               Saved as 'data.mat' in the /dcnm_root/data folder 
               input: dcnm('text-dcnr','edges.csv', <node_vocab.txt>, <observed_metadata.txt>,...
                      <metadata_genfeat.txt>)

INFO: "edges.csv" is a comma separated file where the first two columns refer to the edge identifier
i,j and the third column represents whether an edge exists or not {0,1}. Note that only present
edges need to be listed and all other pairwise edges will be assumed to be absent. See "edges.csv"
in the /dcnm_root/data/relational_modeling/toy subdirectory. The rest of the arguments are optional
and similar to the optional text files for the topic model as discussed above. [Currently visualizations
for the relational model are not fully supported in this current release of DCNM.]

------------------------ RUNNING ANALYSIS: LOADING A MATFILE AND PARAMETERS-------------------------

'matfile' =>   Runs the DCNM analysis given the parameters file
               USAGE: dcnm('matfile', 'some_parameter_file', 'RANDOM_SEED_INTEGER')
               EXAMPLE: dcnm('matfile', 'param_file', '456')

INFO: Please use the param_default.m file as a template for changes to your experiment. Remember not to
include the .m when specifying the name of your parameter file. All aspects of your experiment such as
your save directory, the number of MCMC iterations, and almost any other possible changes can be made
in this file.

'continue' =>  Continues the analysis from a saved dcnm struct from its last saved iteration. 
               input: dcnm('continue','matlab_results_toy_RS1_iter40.mat', <new_save_dir>)


------------------------ SAMPLE ANALYSIS: VARIOUS EXPERIMENTS -------------------------
To run a quick toy bars experiment you can copy and paste the following command
while in the root directory of this toolbox:

The parameter function files are all stored in the experiments folder: /dcnm_root/experiments 
All experiments assume you are running from the dcnm_root directory. Otherwise add the path
and all subpaths using the command: addpath(genpath('/../dcnm_root_location'))

EXPERIMENT: CORRELATED TOY BARS DATASET (1000 documents)
            TRUNCATED MODEL where K=10 (num. of topics) run for 500 iterations as 
            well as modeling correlations.

EXAMPLE:    dcnm('matfile','param_toybars', '1')

EXPERIMENT: TOY GRAPH DATASET (100 nodes)
            RETROSPECTIVE MCMC INFERENCE where intial K=10 (num. of communities) 
            but can continually learn K. Will run for 500 iterations as well as 
            modeling correlations.

EXAMPLE:    dcnm('matfile','param_toygraph', '1')

EXPERIMENT: NSF DATASET (1104 documents)
            RETROSPECTIVE MCMC INFERENCE where intial K=10 (num. of communities) 
            but can continually learn K. Will run for 500 iterations as well as 
            modeling correlations. Specifying these extra files will result in 

EXAMPLE:    dcnm('text-dcnt','nsf_wordcounts.csv','nsf_vocab.txt','nsf_doclist.txt','nsf_metadata.txt','nsf_metadata_gen.txt');
            dcnm('matfile','param_nsf','1')

NOTE: Generating the dataset using the 'text-dcnt' command with the optional files will also automatically create web 
visualizations that can be found in the /web subdirectory within the save directory you specified. Double-clicking on 
the corresponding html files should load the visualization. Visualizations for the relational model not yet developed.

Specifying the vocabulary file will generate the wordcloud visualization. Also needed for the Hinton visualization.
Specifying the doclist will generate the top documents associated with each topic.
Specifying the metadata and metadata_gen files will result in the stacked histogram metadata visualization.
Please see the example text files above to see the format of these files. Any further questions can be forwarded to daeil@cs.brown.edu.  

------------------------ PARAMETERS: SHORT DESCRIPTION OF SOME MODEL VARIABLES-------------------------

%%%% modelParams %%%%% (parameters related to the model (random variables, fixed hyperparameters, num. of topics, etc..)
model.param.K = {Integer}               % Number of initial topics/communities
model.truncate = {0,1}                  % Specify whether you wish to truncate the model (1) or make it retrospective (0)
model.correlations = {0,1}              % Model correlations (i.e learn A, u)
model.fixed.{variables}                 % Stores hyperparameters that are not optimized during inference
model.param.{variables}                 % Stores things such as the number of topics, features, documents
model.learned.{variables}               % Variables that can be learned are stored here
model.saved.{variables}                 % Stores variables at every "outParams.saveParamEvery" number of iterations
model.ll.{variables}                    % Stores loglikelihood values

%%%% outParams %%%%% (parameters related to the data)
outParams.savedir = {filepath}          % Specified the save directory
outParams.savefid = {filename}          % Specified the save name file
outParams.saveEvery = {Integer}         % Specifies how often to save to disk
outParams.saveParamEvery = {Integer}    % Specifies how often to save parameters
outParams.printEvery = {Integer}        % Specifies how often to print relevant progress
outParams.logPrEvery = {Integer}        % Specifies how often to calculate the log likelihood of the model

%%%% algParams %%%%% (parameters related to inference and learning) 
algParams.type = {'mcmc'}               % Currently set to be mcmc, might have a VB option one day
algParams.maxIter = {Integer}           % Number of iterations for MCMC to run 
algParams.sampleFunc = {'@function}     % The function call used for mcmc for topic/relational models
algParams.mcmc.numEss = {Integer}       % Number of times to run the ESS sampler


===========================================================================
http://www.cs.brown.edu/~daeil

Copyright 2011, Brown University, Providence, RI.

                        All Rights Reserved

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose other than its incorporation into a
commercial product is hereby granted without fee, provided that the
above copyright notice appear in all copies and that both that
copyright notice and this permission notice appear in supporting
documentation, and that the name of Brown University not be used in
advertising or publicity pertaining to distribution of the software
without specific, written prior permission.

BROWN UNIVERSITY DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR ANY
PARTICULAR PURPOSE.  IN NO EVENT SHALL BROWN UNIVERSITY BE LIABLE FOR
ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.